﻿using System;
using System.Diagnostics;
using JSchemeSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class PairTest
    {
        [TestMethod]
        public void TestLengthOnLoopedPairs() {
            var p = new Pair(1, null);
            p.TheRest = p;
            Assert.AreEqual(2, SchemeUtils.Length(p));
        }

        [TestMethod]
        public void TestHashCodeOnLoopedPairs() {
            var p = new Pair(1, null);
            p.TheRest = p;
            Assert.AreEqual(p.GetHashCode(), p.GetHashCode());
        }

        [TestMethod]
        public void TestEqualityOnPairContent() {
            var p1 = new Pair(1, 2);
            var p2 = new Pair(1, 2);
            Assert.AreNotSame(p1, p2);
            Assert.AreEqual(p1, p2);
        }

        [TestMethod]
        public void TestEqualityOnLoopedPairs() {
            var p1 = new Pair(null, null);
            p1.TheRest = p1;
            p1.TheFirst = p1;
            var p2 = new Pair(p1, p1);
            Assert.AreEqual(p1, p2);
        }

        [TestMethod]
        public void TestToStringOnLoopedPair() {
            // no loop
            var p1 = new Pair(1, 1);
            Assert.IsFalse(p1.ToString().Contains("error"));

            var p3 = new Pair(2, null);
            p1.TheRest = p3;
            Assert.IsFalse(p1.ToString().Contains("error"));
            p1.TheFirst = p3;
            Assert.IsFalse(p1.ToString().Contains("error"));
            p1.TheRest = 2;
            Assert.IsFalse(p1.ToString().Contains("error"));

            // Loop on TheRest
            p1.TheRest = p1;
            Assert.IsTrue(p1.ToString().Contains("error"));

            // Loop on TheFirst
            p1.TheFirst = p1;
            p1.TheRest = 1;
            Assert.IsTrue(p1.ToString().Contains("error"));

            var p2 = new Pair(p1, 1);
            p1.TheFirst = p2;
            Assert.IsTrue(p1.ToString().Contains("error"));

            p1.TheFirst = 1;
            p1.TheRest = p2;
            Assert.IsTrue(p1.ToString().Contains("error"));
        }

        [TestMethod]
        public void TestLengthOnPair()
        {
            Assert.AreEqual(1, SchemeUtils.Length(new Pair(null, null)));
            Assert.AreEqual(1, SchemeUtils.Length(new Pair(1, null)));
            Assert.AreEqual(1, SchemeUtils.Length(new Pair(null, 1)));
            // Count length using TheRest
            Assert.AreEqual(2, SchemeUtils.Length(new Pair(1, new Pair(null, null))));
            // Do not count length on TheFirst
            Assert.AreEqual(1, SchemeUtils.Length(new Pair(new Pair(null, null), 1)));
        }
    }
}
