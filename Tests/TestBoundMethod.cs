﻿using JSchemeSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Tests {

    public class MethodClass {

        public void NoParamsMethod() {
        }
    }

    [TestClass]
    public class TestBoundMethod {

        private void AssertTypeIsMappedCorrectlyByType(Type t) {
            var mappedType = Method.ToClass(t);
            Assert.AreEqual(mappedType, t, string.Format("{0} mapped to {1}", t.Name, mappedType.Name));
        }

        [TestMethod]
        public void SystemTypesAreMappedCorrectlyByType() {
            AssertTypeIsMappedCorrectlyByType(typeof(void));
            AssertTypeIsMappedCorrectlyByType(typeof(bool));
            AssertTypeIsMappedCorrectlyByType(typeof(char));
            AssertTypeIsMappedCorrectlyByType(typeof(byte));
            AssertTypeIsMappedCorrectlyByType(typeof(short));
            AssertTypeIsMappedCorrectlyByType(typeof(int));
            AssertTypeIsMappedCorrectlyByType(typeof(long));
            AssertTypeIsMappedCorrectlyByType(typeof(float));
            AssertTypeIsMappedCorrectlyByType(typeof(double));
        }

        private void AssertTypeIsMappedCorrectlyByString(string typename, Type expectedType) {
            var mappedType = Method.ToClass(typename);
            Assert.AreEqual(mappedType, expectedType, string.Format("{0} mapped to {1}", typename, mappedType.Name));
        }

        [TestMethod]
        public void SystemTypesAreMappedCorrectlyByString() {
            AssertTypeIsMappedCorrectlyByString("void", typeof(void));
            AssertTypeIsMappedCorrectlyByString("bool", typeof(bool));
            AssertTypeIsMappedCorrectlyByString("char", typeof(char));
            AssertTypeIsMappedCorrectlyByString("byte", typeof(byte));
            AssertTypeIsMappedCorrectlyByString("short", typeof(short));
            AssertTypeIsMappedCorrectlyByString("int", typeof(int));
            AssertTypeIsMappedCorrectlyByString("long", typeof(long));
            AssertTypeIsMappedCorrectlyByString("float", typeof(float));
            AssertTypeIsMappedCorrectlyByString("double", typeof(double));
        }
    }
}