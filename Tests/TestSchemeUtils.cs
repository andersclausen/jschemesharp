﻿using JSchemeSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Tests {

    internal class ConcreteSchemeUtils : SchemeUtils {
        private static readonly StringBuilder Sb = new StringBuilder();
        public StringWriter strWriter = new StringWriter(Sb);

        public ConcreteSchemeUtils() {
            SetErrorStream(strWriter);
        }

        public string ErrorMessage {
            get { return Sb.ToString(); }
        }

        public void CallError(string s, params object[] args) {
            try {
                Error(s, args);
            } catch (Exception) {
                //Eat the exception thrown by Error
            }
        }
        public void CallWarning(string s, params object[] args)
        {
            Warning(s, args);
        }
    }

    [TestClass]
    public class TestSchemeUtils {
        private ConcreteSchemeUtils _su;

        [TestInitialize]
        public void Initialize() {
            _su = new ConcreteSchemeUtils();
        }

        [TestMethod]
        public void TestSimpleError() {
            _su.CallError("foo");
            StringAssert.Contains(_su.ErrorMessage, "foo");
        }

        [TestMethod]
        public void TestFormattedError() {
            _su.CallError("{0} {1}", 123, true);
            StringAssert.Contains(_su.ErrorMessage, "123 True");
        }
        [TestMethod]
        public void TestSimpleWarning() {
            _su.CallWarning("foo");
            StringAssert.Contains(_su.ErrorMessage, "foo");
        }

        [TestMethod]
        public void TestFormattedWarning() {
            _su.CallWarning("{0} {1}", 123, true);
            StringAssert.Contains(_su.ErrorMessage, "123 True");
        }

        [TestMethod]
        public void TestLength() {
            Assert.AreEqual(0, SchemeUtils.Length(null));
            Assert.AreEqual(0, SchemeUtils.Length(new object()));
            var p = new Pair(1, null);
            Assert.AreEqual(1, SchemeUtils.Length(p));
            p.TheRest = new Pair(2, null);
            Assert.AreEqual(2, SchemeUtils.Length(p));
        }
    }
}