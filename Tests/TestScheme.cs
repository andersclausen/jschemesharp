﻿using JSchemeSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

namespace Tests {

    [TestClass]
    public class TestScheme {
        private Scheme _scheme;

        [TestInitialize]
        public void Initialize() {
            _scheme = new Scheme(null);
        }

        [TestCleanup]
        public void Cleanup() {
            _scheme = null;
        }

        [TestMethod]
        public void TestCanCreateScheme() {
            Assert.IsNotNull(_scheme);
        }

        [TestMethod]
        public void TestCanExecuteStringViaInputPort() {
            var result = _scheme.Load(new InputPort("(+ 3 5)"));
            Assert.IsTrue((bool)result);
        }

        [TestMethod]
        public void TestCanExecuteString() {
            var result = _scheme.Load("(+ 3 5)");
            Assert.IsTrue((bool)result);
        }

        [TestMethod]
        public void TestCanGetResult() {
            var result = _scheme.Load("(define a 42)");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.AreEqual(42, (double)a);
        }

        [TestMethod]
        public void TestCanSetVariable() {
            _scheme.Load("(define a 42)");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.AreEqual(42, (double)a);

            _scheme.Load("(define b 84)");
            var b = _scheme.GlobalEnvironment.Lookup("b");
            Assert.AreEqual(84, (double)b);

            _scheme.Load("(set! a b)");
            a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.AreEqual(84, (double)a);
        }

        [TestMethod]
        public void TestCanRunCons() {
            var code = "(let loop ((n 1)) (if (> n 10) '() (cons n (loop (+ n 1)))))";
            var result = _scheme.Load(code);
            Assert.IsTrue((bool)result);
        }

        [TestMethod]
        public void TestCanRunEval() {
            var input = new InputPort("(let loop ((n 1)) (if (> n 10) '() (cons n (loop (+ n 1)))))");
            var result = _scheme.Evaluate(input.Read());
            var stringified = SchemeUtils.Stringify(result);
            Assert.AreEqual("(1 2 3 4 5 6 7 8 9 10)", stringified);
        }

        [TestMethod]
        public void TestCanLoadFile() {
            var scheme = new Scheme(new[] { "Scm/test_load_file.scm" });
            var a = scheme.GlobalEnvironment.Lookup("a");
            Assert.AreEqual(9999, (double)a);
        }

        [TestMethod]
        public void TestR4RSTestNorvigCompareRecursiveNonRecursiveStringify() {
            RunStringifyComparison("https://bitbucket.org/andersclausen/schemetests/raw/master/modified/r4rstest_norvig.scm", 0);
        }

        [TestMethod]
        public void TestR4RSTestGnuCompareRecursiveNonRecursiveStringify() {
            RunStringifyComparison("https://bitbucket.org/andersclausen/schemetests/raw/master/modified/r4rstest_gnu.scm", 1);
        }

        private void RunStringifyComparison(string fileName, int regressionCount) {
            var origState = SchemeUtils.RecursiveStringify;

            var sw = new Stopwatch();
            SchemeUtils.RecursiveStringify = true;
            RunR4RSTest(fileName, new string[] { }, new string[] { }, currentRegressionCount: regressionCount, stopwatch: sw);
            var recursiveTicks = sw.ElapsedTicks;

            SchemeUtils.RecursiveStringify = false;
            RunR4RSTest(fileName, new string[] { }, new string[] { }, currentRegressionCount: regressionCount, stopwatch: sw);
            var nonRecursiveTicks = sw.ElapsedTicks;

            SchemeUtils.RecursiveStringify = origState;

            Debug.WriteLine("Rcursive: {0} ticks, non recursive {1} ticks", recursiveTicks, nonRecursiveTicks);
        }

        /// <summary>
        /// Skipping continuation tests since the original implementation only supports continuations as escape procedures.
        /// </summary>
        [TestMethod]
        public void TestR4RSTestNorvig() {
            RunR4RSTest("https://bitbucket.org/andersclausen/schemetests/raw/master/modified/r4rstest_norvig.scm",
                new string[] { }, new string[] { }, currentRegressionCount: 0);
        }

        /// <summary>
        /// Skipping continuation tests since the original implementation only supports continuations as escape procedures.
        /// </summary>
        [TestMethod]
        public void TestR4RSTestGnu() {
            RunR4RSTest("https://bitbucket.org/andersclausen/schemetests/raw/master/modified/r4rstest_gnu.scm",
                new string[] { }, new[] { "(test-sc4)", "(test-delay)" }, currentRegressionCount: 1);
        }

        private void RunR4RSTest(string url, string[] excludeList, string[] extraTests, int currentRegressionCount, Stopwatch stopwatch = null) {
            const string fileNameDownloaded = @"r4rstest_downloaded.scm";
            const string fileNameModified = @"r4rstest.scm";

            var webClient = new WebClient();
            // https://social.msdn.microsoft.com/Forums/vstudio/en-US/31a7deb0-b135-4163-b884-3efa35ba6994/webclient-403-forbidden
            webClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
            webClient.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            webClient.DownloadFile(url, fileNameDownloaded);

            var line = string.Empty;
            using (var outFile = new StreamWriter(fileNameModified))
            using (var inFile = new StreamReader(fileNameDownloaded)) {
                while ((line = inFile.ReadLine()) != null) {
                    foreach (var item in excludeList) {
                        if (line.Contains(item)) {
                            Debug.WriteLine(string.Format("Excluded: \"{0}\"", line));
                            line = string.Format(";{0} ;JSchemeSharp fail here!", line);
                            break;
                        }
                    }

                    outFile.WriteLine(line);
                }
            }

            var errors = string.Empty;

            {
                var stringBuilder = new StringBuilder();
                var stringWriter = new StringWriter(stringBuilder);
                if (stopwatch != null) {
                    stopwatch.Reset();
                    stopwatch.Start();
                }

                var scheme = new Scheme(new[] { fileNameModified }, stringWriter);
                foreach (var test in extraTests) {
                    scheme.Load(test);
                }

                if (stopwatch != null) {
                    stopwatch.Stop();
                }

                scheme.Dispose();

                var results = stringWriter.GetStringBuilder().ToString()
                    .Split(new string[] { System.Environment.NewLine, "\n" }, System.StringSplitOptions.None);

                var errSet = new HashSet<string>();
                foreach (var result in results) {
                    if (result.StartsWith("error:")) {
                        errSet.Add(result);
                    }
                }

                Assert.IsTrue(errSet.Count <= currentRegressionCount, "Regression in r4rstest, expected {0} errors, got {1}", currentRegressionCount, errSet.Count);

                if (errSet.Count < currentRegressionCount) {
                    Debug.WriteLine("Expected regression count improved. Expected {0} errors, got {1}. Update the test!", currentRegressionCount, errSet.Count);
                }

                if (errSet.Count > 0) {
                    Debug.WriteLine("{0} errors: (SECTION (got expected (call)))", errSet.Count);
                }

                foreach (var err in errSet) {
                    //Assert.Fail(string.Format("(SECTION (got expected (call))) {0}", result));
                    // Note! Select the test and click "Output" to see messages... (well hidden).
                    Debug.WriteLine(err);
                }
            }

            File.Delete(fileNameDownloaded);
            File.Delete(fileNameModified);
            File.Delete(@"tmp1");
            File.Delete(@"tmp2");
        }

        [TestMethod]
        public void TestDoubleGetsLeadingZero() {
            var input = new InputPort("(cdr '(3 .4))");
            var result = _scheme.Evaluate(input.Read());
            var stringified = SchemeUtils.Stringify(result);
            Assert.AreEqual("(0.4)", stringified);
        }

        [TestMethod]
        public void TestCanCreateObject() {
            _scheme.Load("(define a (new \"System.Int32\"))");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.IsInstanceOfType(a, typeof(int));
        }

        [TestMethod]
        public void TestCanCreateClass() {
            _scheme.Load("(define a (class \"System.Int32\"))");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.IsInstanceOfType(a, typeof(Type));
            Assert.AreEqual("System.Int32", a.ToString());
        }

        [TestMethod]
        public void TestCanRunNonStaticMethodWithoutArguments() {
            _scheme.Load("(define int-string (method \"ToString\" \"System.Int32\"))");
            _scheme.Load("(define a (int-string (new \"System.Int32\")))");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.IsInstanceOfType(a, typeof(string));
            Assert.AreEqual("0", (string)a);
        }

        [TestMethod]
        public void TestCanRunNonStaticMethodWithArgumentOnExternallyDefinedObject() {
            var i = 42;
            _scheme.GlobalEnvironment.Define("theint", i);
            _scheme.Load("(define int-compare (method \"CompareTo\" \"System.Int32\" \"System.Int32\"))");
            _scheme.Load("(define a (int-compare tHeInt 41))"); // Note case
            _scheme.Load("(define b (int-compare ThEiNt 42))"); // -- || --
            _scheme.Load("(define c (int-compare theint 43))"); // -- || --

            var a = _scheme.GlobalEnvironment.Lookup("a");
            var b = _scheme.GlobalEnvironment.Lookup("b");
            var c = _scheme.GlobalEnvironment.Lookup("c");

            Assert.IsInstanceOfType(a, typeof(int));
            Assert.IsInstanceOfType(b, typeof(int));
            Assert.IsInstanceOfType(c, typeof(int));

            Assert.AreEqual(1, (int)a);
            Assert.AreEqual(0, (int)b);
            Assert.AreEqual(-1, (int)c);
        }

        [TestMethod]
        public void TestCanRunStaticMethodWithArguments() {
            _scheme.Load("(define days-in-month (method \"DaysInMonth\" \"System.DateTime\" \"System.Int32\" \"System.Int32\"))");
            _scheme.Load("(define a (days-in-month 2000 2))");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.IsInstanceOfType(a, typeof(int));
            Assert.AreEqual(DateTime.DaysInMonth(2000, 2), (int)a);
        }

        public class TestClass {
            public int I { get; private set; }

            public int Add(int i) {
                I += i;
                return I;
            }

            public int Add2(int i, long j) {
                I += i;
                I += (int)j;
                return I;
            }

            public TestClass(int i) {
                I = i;
            }
        }

        [TestMethod]
        public void TestCanCallMethodOnOwnClassObject() {
            var tc = new TestClass(42);
            _scheme.GlobalEnvironment.Define("testclass", tc);
            _scheme.Load(string.Format("(define testclass-add (method \"Add\" \"{0}\" \"System.Int32\"))", SchemeUtils.QualifiedName(typeof(TestClass))));
            _scheme.Load("(define a (testclass-add testclass 2))");

            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.IsInstanceOfType(a, typeof(int));
            Assert.AreEqual(44, (int)a);
            Assert.AreEqual(44, tc.I);
        }

        [TestMethod]
        public void TestCanCallMethodBoundWithBindMethod() {
            _scheme.BindMethod(new TestClass(42), "Add2", "testclass-addtwo", "testclass", typeof(int), typeof(long));
            _scheme.Load("(define a (testclass-addtwo testclass 3 4))");

            var a = _scheme.GlobalEnvironment.Lookup("a");
            Assert.IsInstanceOfType(a, typeof(int));
            Assert.AreEqual(49, (int)a);
        }

        [TestMethod]
        public void TestCanCallMethodOnDifferentObjects() {
            var tc1 = new TestClass(42);
            var tc2 = new TestClass(84);
            _scheme.GlobalEnvironment.Define("tc1", tc1);
            _scheme.GlobalEnvironment.Define("tc2", tc2);

            _scheme.BindMethod(typeof(TestClass), "Add2", "testclass-addtwo", typeof(int), typeof(long));
            _scheme.Load("(define a (testclass-addtwo tc1 3 4))");
            _scheme.Load("(define b (testclass-addtwo tc2 3 4))");

            var a = _scheme.GlobalEnvironment.Lookup("a");
            var b = _scheme.GlobalEnvironment.Lookup("b");
            Assert.IsInstanceOfType(a, typeof(int));
            Assert.IsInstanceOfType(b, typeof(int));
            Assert.AreEqual(49, (int)a);
            Assert.AreEqual(91, (int)b);
            Assert.AreEqual(49, tc1.I);
            Assert.AreEqual(91, tc2.I);
        }

        [TestMethod]
        public void TestEquivalence() {
            _scheme.Load("(define a (= 2 3))");
            _scheme.Load("(define b (= 1.7 1.7))");
            var a = _scheme.GlobalEnvironment.Lookup("a");
            var b = _scheme.GlobalEnvironment.Lookup("b");
            Assert.IsInstanceOfType(a, typeof(bool));
            Assert.IsInstanceOfType(b, typeof(bool));
            Assert.IsFalse((bool)a);
            Assert.IsTrue((bool)b);
        }

        [TestMethod]
        public void TestEQQ() {
            _scheme.Load("(define a '(5 7))");
            _scheme.Load("(define b '(5 7))");
            _scheme.Load("(define c '())");
            _scheme.Load("(define d '())");
            _scheme.Load("(define f1 (eq? a b))");
            _scheme.Load("(define b a)");
            _scheme.Load("(define t1 (eq? a b))");
            _scheme.Load("(define t2 (eq? c d))");
            var f1 = _scheme.GlobalEnvironment.Lookup("f1");
            var t1 = _scheme.GlobalEnvironment.Lookup("t1");
            var t2 = _scheme.GlobalEnvironment.Lookup("t2");
            Assert.IsFalse((bool)f1);
            Assert.IsTrue((bool)t1);
            Assert.IsTrue((bool)t2);
        }

        [TestMethod]
        public void TestCompareEQQAndEQVQ() {
            CompareEQs("'#f", "'#f");
            CompareEQs("\"cde\"", "\"cdf\"");
            CompareEQs("\"cde\"", "\"cde\"");
        }

        private void CompareEQs(string first, string second) {
            _scheme.Load(string.Format("(define a (eq? {0} {1}))", first, second));
            _scheme.Load(string.Format("(define b (eqv? {0} {1}))", first, second));
            _scheme.Load(string.Format("(define c (eq? (eq? {0} {1}) (eqv? {0} {1})))", first, second));
            var a = _scheme.GlobalEnvironment.Lookup("a");
            var b = _scheme.GlobalEnvironment.Lookup("b");
            var c = _scheme.GlobalEnvironment.Lookup("c");
            Assert.AreEqual((bool)a, (bool)b, string.Format("(define a/b (eq?/eqv? {0} {1}))", first, second));
            Assert.IsTrue((bool)c, string.Format("(define c (eq? (eq? {0} {1}) (eqv? {0} {1}))", first, second));
        }
    }
}