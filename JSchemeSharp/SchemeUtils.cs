/**  @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
* This altered version of JScheme is licenced under the MIT license:
* https://opensource.org/licenses/MIT as allowed under point 4 of the
* original license.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace JSchemeSharp {

    public abstract class SchemeUtils {
        public static readonly double ZERO = 0.0;
        public static readonly double ONE = 1.0;
        private static TextWriter _errorStream = Console.Error;
        public static bool RecursiveStringify = true;

        public class StringifyArgs {
            public object O;
            public bool Quoted;
            public StringBuilder Buffer;
            public Action<StringBuilder> PostAction;

            public StringifyArgs(object o, bool quoted, StringBuilder buffer, Action<StringBuilder> action) {
                O = o;
                Quoted = quoted;
                Buffer = buffer;
                PostAction = action;
            }

            public StringifyArgs(StringBuilder buffer, Action<StringBuilder> action) : this(null, false, buffer, action) {
            }

            public StringifyArgs(object o, bool quoted, StringBuilder buffer) : this(0, quoted, buffer, null) {
            }
        }

        //////////////// Conversion Routines ////////////////
        // The following convert or coerce objects to the right type.

        /// <summary>
        /// Convert Scheme object to bool. Only #f is false, others are true.
        /// </summary>
        /// <param name="o">The object</param>
        /// <returns>The bool converted</returns>
        public static bool Truth(object o) {
            if (o is bool) {
                return (bool)o;
            }

            return true;
        }

        /// <summary>
        /// Converts a Scheme object to a double, or calls error.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The double or error</returns>
        public static double Num(object o) {
            return o.IsNumber() ? Convert.ToDouble(o, CultureInfo.InvariantCulture) : Num(Error("expected a number, got: {0}", o));
        }

        /// <summary>
        /// Converts a Scheme object to a char, or calls error.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The char or error</returns>
        public static char Chr(object o) {
            if (o is char) {
                return Convert.ToChar(o);
            }

            return Chr(Error("expected a char, got: {0}", o));
        }

        /// <summary>
        /// Coerces a Scheme object to a Scheme string, which is a char[].
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The Scheme string</returns>
        public static char[] Str(object o) {
            if (o is char[]) {
                return (char[])o;
            }

            return Str(Error("expected a string, got: {0}", o));
        }

        /// <summary>
        /// Coerces a Scheme object to a Scheme symbol, which is a string.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The Scheme symbol</returns>
        public static string Sym(object o) {
            if (o is string) {
                return (string)o;
            }

            return Sym(Error("expected a symbol, got: {0}", o));
        }

        /// <summary>
        /// Coerces a Scheme object to a Scheme vector, which is a object[].
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The Scheme vector</returns>
        public static object[] Vector(object o) {
            if (o is object[]) {
                return (object[])o;
            }

            return Vector(Error("expected a vector, got: {0}", o));
        }

        /// <summary>
        /// Coerces a Scheme object to a Scheme input port, which is an InputPort.
        /// If the argument is null, returns interpreter.input.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <returns>The Scheme input port</returns>
        public static InputPort InPort(object o, Scheme interpreter) {
            if (o == null) {
                return interpreter.Input;
            }

            if (o is InputPort) {
                return (InputPort)o;
            }

            return InPort(Error("expected an input port, got: {0}", o), interpreter);
        }

        /// <summary>
        /// Coerces a Scheme object to a Scheme output port, which is a TextWriter.
        /// If the argument is null, returns System.out.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <returns>The Scheme oputput port</returns>
        public static TextWriter OutPort(object o, Scheme interpreter) {
            if (o == null) {
                return interpreter.Output;
            }

            if (o is TextWriter) {
                return (TextWriter)o;
            }

            return OutPort(Error("expected an output port, got: {0}", o), interpreter);
        }

        //////////////// Error Routines ////////////////

        /// <summary>
        /// A continuable error. Prints an error message and then prompts for
        /// a value to eval and return.
        /// </summary>
        /// <param name="message">The error message</param>
        /// <returns>Throws <see cref="SystemException"/> with the message</returns>
        public static object Error(string message) {
            _errorStream.WriteLine("**** ERROR: " + message);
            throw new SystemException(message);
        }

        public static object Error(string message, params object[] args) {
            Error(string.Format(CultureInfo.InvariantCulture, message, args));
            return null;
        }

        public static object Warning(string message) {
            _errorStream.WriteLine("**** WARNING: " + message);
            return "<warn>";
        }

        public static object Warning(string message, params object[] args) {
            return Warning(string.Format(CultureInfo.InvariantCulture, message, args));
        }

        //////////////// Basic manipulation Routines ////////////////
        // The following are used throughout the code.

        /// <summary>
        /// Like Common Lisp first; car of a Pair, or null for anything else.
        /// </summary>
        /// <param name="o">The <see cref="Pair"/> or list</param>
        /// <returns>The first</returns>
        public static object First(object o) {
            return (o is Pair) ? ((Pair)o).TheFirst : null;
        }

        /// <summary>
        /// Like Common Lisp rest; car of a Pair, or null for anything else.
        /// </summary>
        /// <param name="x">The <see cref="Pair"/> or list</param>
        /// <returns>The rest or null</returns>
        public static object Rest(object x) {
            return (x is Pair) ? ((Pair)x).TheRest : null;
        }

        /// <summary>
        /// Like Common Lisp (setf (first ...
        /// </summary>
        /// <param name="x">The list</param>
        /// <param name="y">The first</param>
        /// <returns>The list with the first set</returns>
        public static object SetFirst(object x, object y) {
            return (x is Pair)
                ? ((Pair)x).TheFirst = y
                : Error("Attempt to set-car of a non-Pair: {0}", Stringify(x));
        }

        /// <summary>
        /// Like Common Lisp (setf (rest ...
        /// </summary>
        /// <param name="x">The list</param>
        /// <param name="y">The rest</param>
        /// <returns>The list with the rest set</returns>
        public static object SetRest(object x, object y) {
            return (x is Pair)
                ? ((Pair)x).TheRest = y
                : Error("Attempt to set-cdr of a non-Pair: {0}", Stringify(x));
        }

        /// <summary>
        /// Like Common Lisp second.
        /// </summary>
        /// <param name="o">The list</param>
        /// <returns>The second</returns>
        public static object Second(object o) {
            return First(Rest(o));
        }

        /// <summary>
        /// Like Common Lisp third.
        /// </summary>
        /// <param name="o">The list</param>
        /// <returns>The third</returns>
        public static object Third(object o) {
            return First(Rest(Rest(o)));
        }

        /// <summary>
        /// Creates a two element list.
        /// </summary>
        /// <param name="a">The first</param>
        /// <param name="b">The second</param>
        /// <returns>The new <see cref="Pair"/></returns>
        public static Pair List(object a, object b) {
            return new Pair(a, new Pair(b, null));
        }

        /// <summary>
        /// Creates a one element list.
        /// </summary>
        /// <param name="o">The element</param>
        /// <returns>The list</returns>
        public static Pair List(object o) {
            return new Pair(o, null);
        }

        /// <summary>
        /// listStar(args) is like Common Lisp (apply #'list* args)
        /// </summary>
        /// <param name="arguments">The args list</param>
        /// <returns>The list</returns>
        public static object ListStar(object arguments) {
            if (Rest(arguments) == null) {
                return First(arguments);
            }

            return Cons(First(arguments), ListStar(Rest(arguments)));
        }

        /// <summary>
        /// cons(x, y) is the same as new Pair(x, y).
        /// </summary>
        /// <param name="a">The first</param>
        /// <param name="b">The rest</param>
        /// <returns>The new <see cref="Pair"/></returns>
        public static Pair Cons(object a, object b) {
            return new Pair(a, b);
        }

        /// <summary>
        /// Reverse the elements of a list.
        /// </summary>
        /// <param name="o">The scheme list object</param>
        /// <returns>The reversed list</returns>
        public static object Reverse(object o) {
            object result = null;
            while (o is Pair) {
                result = Cons(First(o), result);
                o = Rest(o);
            }

            return result;
        }

        /// <summary>
        /// Check if two objects are equal.
        /// </summary>
        /// <param name="x">The left object</param>
        /// <param name="y">The right object</param>
        /// <returns>True if equal, false otherwise</returns>
        public static bool Equal(object x, object y) {
            if (x == null || y == null) {
                return x == y;
            }

            if (x is char[]) {
                if (!(y is char[])) {
                    return false;
                }

                char[] xc = (char[])x, yc = (char[])y;
                if (xc.Length != yc.Length) {
                    return false;
                }

                for (var i = xc.Length - 1; i >= 0; i--) {
                    if (xc[i] != yc[i]) {
                        return false;
                    }
                }

                return true;
            }

            if (x is object[]) {
                if (!(y is object[])) {
                    return false;
                }

                object[] xo = (object[])x, yo = (object[])y;
                if (xo.Length != yo.Length) {
                    return false;
                }

                for (var i = xo.Length - 1; i >= 0; i--) {
                    if (!Equal(xo[i], yo[i])) {
                        return false;
                    }
                }

                return true;
            }

            return x.Equals(y);
        }

        /// <summary>
        /// Check if two objects are == or are equal numbers or characters.
        /// </summary>
        /// <param name="x">The left object</param>
        /// <param name="y">The right object</param>
        /// <returns>True if equal, false otherwise</returns>
        public static bool Eqv(object x, object y) {
            return x == y
                   || (x is double && x.Equals(y))
                   || (x is char && x.Equals(y))
                   || (x is bool && x.Equals(y));
        }

        /// <summary>
        /// The Length of a list, or zero for a non-list.
        /// </summary>
        /// <param name="o">The Scheme list object</param>
        /// <returns>The list length. The length is 0 for any object that is not a list object/Pair. -1 if the list contains itself.</returns>
        public static int Length(object o) {
            var seen = new HashSet<object>();
            var len = 0;
            while (o is Pair) {
                len++;
                if (seen.Contains(o)) {
                    break;
                }

                seen.Add(o);
                o = ((Pair)o).TheRest;
            }

            return len;
        }

        /// <summary>
        /// Convert a list of characters to a Scheme string, which is a char[].
        /// </summary>
        /// <param name="chars">The list of chars</param>
        /// <returns>The Scheme string</returns>
        public static char[] ListToString(object chars) {
            var length = Length(chars);
            if (length == -1) {
                throw new InvalidOperationException("Cannot count a list that contains itself.");
            }

            var str = new char[length];
            for (var i = 0; chars is Pair; i++) {
                str[i] = Chr(First(chars));
                chars = Rest(chars);
            }

            return str;
        }

        /// <summary>
        /// Convert a list of Objects to a Scheme vector, which is a object[].
        /// </summary>
        /// <param name="objs">The list of objects</param>
        /// <returns>The Scheme vector</returns>
        public static object[] ListToVector(object objs) {
            var length = Length(objs);
            if (length == -1) {
                throw new InvalidOperationException("Cannot count a list that contains itself.");
            }

            var vec = new object[length];
            for (var i = 0; objs is Pair; i++) {
                vec[i] = First(objs);
                objs = Rest(objs);
            }

            return vec;
        }

        /// <summary>
        /// Write the object to a port.  If quoted is true, use "str" and #\c,
        /// otherwise use str and c.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <param name="port">The output port</param>
        /// <param name="quoted">True if quoted</param>
        /// <returns></returns>
        public static object Write(object o, TextWriter port, bool quoted) {
            port.Write(Stringify(o, quoted));
            port.Flush();
            return o;
        }

        /// <summary>
        /// Convert a vector to a List.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The pair</returns>
        public static Pair VectorToList(object o) {
            if (o is object[]) {
                var vec = (object[])o;
                Pair result = null;
                for (var i = vec.Length - 1; i >= 0; i--) {
                    result = Cons(vec[i], result);
                }

                return result;
            }

            Error("expected a vector, got: {0}", o);
            return null;
        }

        protected static void Stringify(object o, bool quoted, StringBuilder buffer) {
            if (RecursiveStringify) {
                StringifyRecursive(o, quoted, buffer);
            } else {
                StringifyLooped(o, quoted, buffer);
            }
        }

        /// <summary>
        /// Convert a Scheme object to its printed representation, as a C# (formerly java) string
        /// (not a Scheme string). If quoted is true, use "str" and #\c, otherwise use str and c.
        /// You need to pass in a StringBuilder that is used to accumulate the results. (If the
        /// interface didn't work that way, the system would use lots of little internal
        /// StringBuilders.But note that you can still call <see cref="Stringify"/> and a
        /// new StringBuilder will be created for you.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <param name="quoted">True if quoted</param>
        /// <param name="buffer">The <see cref="StringBuilder"/> object</param>
        protected static void StringifyRecursive(object o, bool quoted, StringBuilder buffer) {
            if (o == null) {
                buffer.Append("()");
            } else if (o is double) {
                var d = Convert.ToDouble(o, CultureInfo.InvariantCulture);
                if (Math.Round(d) == d) {
                    buffer.Append((long)d);
                } else {
                    buffer.AppendFormat(CultureInfo.InvariantCulture, "{0}", d);
                }
            } else if (o is char) {
                if (quoted) {
                    buffer.Append("#\\");
                }

                buffer.Append(o);
            } else if (o is Pair) {
                ((Pair)o).StringifyPair(quoted, buffer);
            } else if (o is char[]) {
                var chars = (char[])o;
                if (quoted) {
                    buffer.Append('"');
                }

                foreach (var c in chars) {
                    if (quoted && c == '"') {
                        buffer.Append('\\');
                    }

                    buffer.Append(c);
                }

                if (quoted) {
                    buffer.Append('"');
                }
            } else if (o is object[]) {
                var v = (object[])o;
                buffer.Append("#(");
                for (var i = 0; i < v.Length; i++) {
                    Stringify(v[i], quoted, buffer);
                    if (i != v.Length - 1) {
                        buffer.Append(' ');
                    }
                }

                buffer.Append(')');
            } else if ((o is bool) && (bool)o) {
                buffer.Append("#t");
            } else if ((o is bool) && !(bool)o) {
                buffer.Append("#f");
            } else {
                buffer.Append(o);
            }
        }

        protected static void StringifyLooped(object _o, bool _quoted, StringBuilder _buffer) {
            var stack = new Stack<StringifyArgs>();

            stack.Push(new StringifyArgs(_o, _quoted, _buffer));

            while (true) {
                if (stack.Count == 0) {
                    break;
                }

                var args = stack.Pop();

                var o = args.O;
                var quoted = args.Quoted;
                var buffer = args.Buffer;
                var postAction = args.PostAction;

                if (o == null && postAction == null) {
                    buffer.Append("()");
                } else if (o is double) {
                    var d = Convert.ToDouble(o, CultureInfo.InvariantCulture);
                    if (Math.Round(d) == d) {
                        buffer.Append((long)d);
                    } else {
                        buffer.AppendFormat(CultureInfo.InvariantCulture, "{0}", d);
                    }
                } else if (o is char) {
                    if (quoted) {
                        buffer.Append("#\\");
                    }

                    buffer.Append(o);
                } else if (o is Pair) {
                    ((Pair)o).StringifyPairLooped(quoted, buffer, stack);
                } else if (o is char[]) {
                    var chars = (char[])o;
                    if (quoted) {
                        buffer.Append('"');
                    }

                    foreach (var c in chars) {
                        if (quoted && c == '"') {
                            buffer.Append('\\');
                        }

                        buffer.Append(c);
                    }

                    if (quoted) {
                        buffer.Append('"');
                    }
                } else if (o is object[]) {
                    var v = (object[])o;
                    buffer.Append("#(");

                    stack.Push(new StringifyArgs(buffer, (sb) => { sb.Append(')'); }));

                    for (var i = v.Length - 1; i >= 0; i--) {
                        if (i != v.Length - 1) {
                            stack.Push(new StringifyArgs(buffer, (sb) => { sb.Append(' '); }));
                        }

                        stack.Push(new StringifyArgs(v[i], quoted, buffer));
                    }
                } else if ((o is bool) && (bool)o) {
                    buffer.Append("#t");
                } else if ((o is bool) && !(bool)o) {
                    buffer.Append("#f");
                } else if (o != null) {
                    buffer.Append(o);
                }

                if (postAction != null) {
                    postAction(buffer);
                }
            }
        }

        /// <summary>
        /// Convert x to a Java string giving its external representation.
        /// Strings and characters are quoted.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The string</returns>
        public static string Stringify(object o) {
            return Stringify(o, true);
        }

        /// <summary>
        /// Convert x to a C# (was Java) string giving its external representation.
        /// Strings and characters are quoted if <paramref name="quoted"/> is true..
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <param name="quoted">True if quoted</param>
        /// <returns>The string</returns>
        protected static string Stringify(object o, bool quoted) {
            var buf = new StringBuilder();
            Stringify(o, quoted, buf);
            return buf.ToString();
        }

        /// <summary>
        /// For debugging purposes, prints output.
        /// </summary>
        /// <param name="o">The Scheme object</param>
        /// <returns>The Scheme object</returns>
        private static object p(object o) {
            Console.Out.WriteLine(Stringify(o));
            return o;
        }

        /// <summary>
        /// For debugging purposes, prints output.
        /// </summary>
        /// <param name="msg">The message</param>
        /// <param name="x">The Scheme object</param>
        /// <returns>The Scheme object</returns>
        private static object p(string msg, object x) {
            Console.Out.WriteLine(msg + ": " + Stringify(x));
            return x;
        }

        protected static void SetErrorStream(TextWriter errorStream) {
            if (errorStream == null) {
                throw new ArgumentNullException("errorStream");
            }

            _errorStream = errorStream;
        }

        /// <summary>
        /// Get the qualified name of <see cref="Type"/>. Needed for getting type of proveded
        /// objects on which methods should be called. The space removal seems necessary for
        /// it to work.
        /// </summary>
        /// <typeparam name="T">The <see cref="Type"/></typeparam>
        /// <returns>The qualified assembly name with spaces removed</returns>
        public static string QualifiedName<T>() {
            return typeof(T).AssemblyQualifiedName.Replace(" ", "");
        }

        /// <summary>
        /// Get the qualified name of <see cref="Type"/>. Needed for getting type of proveded
        /// objects on which methods should be called. The space removal seems necessary for
        /// it to work.
        /// </summary>
        /// <param name="type">The <see cref="Type"/></param>
        /// <returns>The qualified assembly name with spaces removed</returns>
        public static string QualifiedName(Type type) {
            return type.AssemblyQualifiedName.Replace(" ", "");
        }
    }
}