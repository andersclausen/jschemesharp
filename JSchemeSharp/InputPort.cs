/** InputPort is to Scheme as InputStream is to Java.
 * @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace JSchemeSharp {

    public class InputPort : SchemeUtils, IDisposable {
        public static string EOF = "#!EOF";
        private readonly StringBuilder _sb = new StringBuilder();
        private readonly StreamReader _inputStream;
        private bool _isPushedChar;
        private bool _isPushedToken;
        private int _pushedChar = -1;
        private object _pushedToken;

        /// <summary>
        /// Construct an InputPort from an InputStream.
        /// </summary>
        /// <param name="inputString">The input stream string</param>
        public InputPort(string inputString) {
            _inputStream = new StreamReader(inputString.GenerateStreamFromString());
        }

        /// <summary>
        /// Construct an InputPort from a Reader.
        /// </summary>
        /// <param name="inputStream">The <see cref="StreamReader"/></param>
        public InputPort(StreamReader inputStream) {
            _inputStream = inputStream;
        }

        /// <summary>
        /// Read and return a Scheme character or EOF.
        /// </summary>
        /// <returns>A Scheme character or EOF</returns>
        public object ReadChar() {
            try {
                if (_isPushedChar) {
                    _isPushedChar = false;
                    if (_pushedChar == -1) {
                        return EOF;
                    }

                    return (char)_pushedChar;
                }

                var ch = _inputStream.Read();
                if (ch == -1) {
                    return EOF;
                }

                return (char)ch;
            } catch (IOException e) {
                Warning("On input, exception: {0}", e.Message);
                return EOF;
            }
        }

        /// <summary>
        /// Peek at and return the next Scheme character (or EOF).
        /// However, don't consume the character.
        /// </summary>
        /// <returns>The peeked character</returns>
        public object PeekChar() {
            var p = PeekCh();
            if (p == -1) {
                return EOF;
            }

            return (char)p;
        }

        /// <summary>
        /// Push a character back to be re-used later.
        /// </summary>
        /// <param name="ch">The character to push</param>
        /// <returns>The result of the assignment</returns>
        private int PushChar(int ch) {
            _isPushedChar = true;
            return _pushedChar = ch;
        }

        /// <summary>
        /// Pop off the previously pushed character.
        /// </summary>
        /// <returns>The previously pushed character</returns>
        private int PopChar() {
            _isPushedChar = false;
            return _pushedChar;
        }

        /// <summary>
        /// Peek at and return the next Scheme character as an int, -1 for EOF.
        /// However, don't consume the character.
        /// </summary>
        /// <returns>The peeked character as int</returns>
        public int PeekCh() {
            try {
                return _isPushedChar ? _pushedChar : PushChar(_inputStream.Read());
            } catch (IOException e) {
                Warning("On input, exception: {0}", e.Message);
                return -1;
            }
        }

        /// <summary>
        /// Read and return a Scheme expression, or EOF.
        /// </summary>
        /// <returns>The read Scheme expression</returns>
        public object Read() {
            try {
                var token = NextToken();
                var tokenString = token is string ? (string)token : string.Empty;

                if (tokenString == "(") {
                    return ReadTail(false);
                }

                if (tokenString == ")") {
                    Warning("Extra ) ignored.");
                    return Read();
                }

                if (tokenString == ".") {
                    Warning("Extra . ignored.");
                    return Read();
                }

                if (tokenString == "'") {
                    return List("quote", Read());
                }

                if (tokenString == "`") {
                    return List("quasiquote", Read());
                }

                if (tokenString == ",") {
                    return List("unquote", Read());
                }

                if (tokenString == ",@") {
                    return List("unquote-splicing", Read());
                }

                return token;
            } catch (IOException e) {
                Warning("On input, exception: {0}", e.Message);
                return EOF;
            }
        }

        /// <summary>
        /// Close the port. Return TRUE if ok.
        /// </summary>
        /// <returns>True if OK</returns>
        public object Close() {
            try {
                _inputStream.Close();
                return true;
            } catch (IOException e) {
                return Error("IOException: {0}", e.Message);
            }
        }

        /// <summary>
        /// Is the argument the EOF object?
        /// </summary>
        /// <param name="o"></param>
        /// <returns>True if the argument is the EOF object</returns>
        public static bool IsEof(object o) {
            return o == EOF;
        }

        private object ReadTail(bool dotOK) {
            var token = NextToken();
            var tokenString = token is string ? (string)token : string.Empty;

            if (tokenString == EOF) {
                return Error("EOF during Read.");
            }

            if (tokenString == ")") {
                return null;
            }

            if (tokenString == ".") {
                var result = Read();
                token = NextToken();
                tokenString = token is string ? (string)token : string.Empty;

                if (tokenString != ")") {
                    Warning("Where's the ')'? Got {0} after '.'", tokenString);
                }

                return result;
            }

            _isPushedToken = true;
            _pushedToken = token;
            return Cons(Read(), ReadTail(true));
        }

        private object NextToken() {
            int ch;

            // See if we should re-use a pushed char or token
            if (_isPushedToken) {
                _isPushedToken = false;
                return _pushedToken;
            }

            if (_isPushedChar) {
                ch = PopChar();
            } else {
                ch = _inputStream.Read();
            }

            // Skip whitespace
            while (char.IsWhiteSpace((char)ch)) {
                ch = _inputStream.Read();
            }

            // See what kind of non-white character we got
            switch (ch) {
                case -1:
                    return EOF;

                case '(':
                    return "(";

                case ')':
                    return ")";

                case '\'':
                    return "'";

                case '`':
                    return "`";

                case ',':
                    ch = _inputStream.Read();
                    if (ch == '@') {
                        return ",@";
                    }

                    PushChar(ch);
                    return ",";

                case ';':
                    // Comment: skip to end of line and then Read next token
                    while (ch != -1 && ch != '\n' && ch != '\r') {
                        ch = _inputStream.Read();
                    }

                    return NextToken();

                case '"':
                    // Strings are represented as char[]
                    _sb.Length = 0;
                    while ((ch = _inputStream.Read()) != '"' && ch != -1) {
                        _sb.Append((char)((ch == '\\') ? _inputStream.Read() : ch));
                    }

                    if (ch == -1) {
                        Warning("EOF inside of a string.");
                    }

                    return _sb.ToString().ToCharArray();

                case '#':
                    switch (ch = _inputStream.Read()) {
                        case 't':
                        case 'T':
                            return true;

                        case 'f':
                        case 'F':
                            return false;

                        case '(':
                            PushChar('(');
                            return ListToVector(Read());

                        case '\\':
                            ch = _inputStream.Read();
                            if (ch == 's' || ch == 'S' || ch == 'n' || ch == 'N') {
                                PushChar(ch);
                                var token = NextToken();
                                var tokenString = token is string ? (string)token : string.Empty;

                                if (tokenString == "space") {
                                    return ' ';
                                }

                                if (tokenString == "newline") {
                                    return '\n';
                                }

                                _isPushedToken = true;
                                _pushedToken = token;
                                return (char)ch;
                            }

                            return (char)ch;

                        case 'e':
                        case 'i':
                        case 'd':
                            return NextToken();

                        case 'b':
                        case 'o':
                        case 'x':
                            Warning("#{0} not implemented, ignored.", (char)ch);
                            return NextToken();

                        default:
                            Warning("#{0} not recognized, ignored.", (char)ch);
                            return NextToken();
                    }

                default:
                    _sb.Length = 0;
                    var c = ch;
                    do {
                        _sb.Append((char)ch);
                        ch = _inputStream.Read();
                    } while (!char.IsWhiteSpace((char)ch) && ch != -1 &&
                             ch != '(' && ch != ')' && ch != '\'' && ch != ';'
                             && ch != '"' && ch != ',' && ch != '`');

                    PushChar(ch);

                    // Try potential numbers, but catch any format errors.
                    if (c == '.' || c == '+' || c == '-' || (c >= '0' && c <= '9')) {
                        try {
                            return Convert.ToDouble(_sb.ToString(), CultureInfo.InvariantCulture);
                        } catch (FormatException) { } catch (OverflowException) { }
                    }

                    return string.Intern(_sb.ToString().ToLower());
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (disposing) {
                if (_inputStream != null) {
                    _inputStream.Close();
                }
            }
        }
    }
}