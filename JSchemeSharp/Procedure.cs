/**  @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html  **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

namespace JSchemeSharp {

    public abstract class Procedure : SchemeUtils {
        public string Name { get; set; }

        protected Procedure() {
            Name = "anonymous procedure";
        }

        public override string ToString() {
            return "{" + Name + "}";
        }

        public abstract object Apply(Scheme interpreter, object arguments);

        /// <summary>
        /// /** Coerces a Scheme object to a procedure. **/
        /// </summary>
        /// <param name="x">The Scheme object</param>
        /// <returns>The <see cref="Procedure"/> or error</returns>
        public static Procedure Proc(object x) {
            if (x is Procedure) {
                return (Procedure)x;
            }

            return Proc(Error("Not a procedure: {0}", Stringify(x)));
        }
    }
}