/** Environments allow you to look up the value of a variable, given
 * its name.  Keep a list of variables and values, and a pointer to
 * the parent environment.  If a variable list ends in a symbol rather
 * than null, it means that symbol is bound to the remainder of the
 * values list.
 * @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html */

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

namespace JSchemeSharp {

    public sealed class Environment : SchemeUtils {
        public Environment Parent { get; set; }
        public object Values { get; set; }
        public object Variables { get; set; }

        /// <summary>
        /// A constructor to extend an environment with var/val pairs.
        /// </summary>
        /// <param name="variables">Variables</param>
        /// <param name="values">Values</param>
        /// <param name="parent">Parent environment</param>
        public Environment(object variables, object values, Environment parent) {
            Variables = variables;
            Values = values;
            Parent = parent;
            if (!NumberArgsOk(variables, values)) {
                Warning("wrong number of arguments: expected {0} got {1}.", variables, values);
            }
        }

        /// <summary>
        /// Construct an empty environment: no bindings.
        /// </summary>
        public Environment() {
        }

        /// <summary>
        /// Find the value of a symbol, in this environment or a parent.
        /// </summary>
        /// <param name="symbol">The symbol</param>
        /// <returns>The value</returns>
        public object Lookup(string symbol) {
            object varList = Variables, valList = Values;

            // See if the symbol is bound locally
            while (varList != null) {
                var firstString = First(varList) as string;
                if (firstString != null && firstString == symbol) {
                    return First(valList);
                }

                var varListString = varList as string;
                if (varListString != null && varListString == symbol) {
                    return valList;
                }

                varList = Rest(varList);
                valList = Rest(valList);
            }

            // If not, try to look for the parent
            if (Parent != null) {
                return Parent.Lookup(symbol);
            }

            return Error("Unbound variable: {0}", symbol);
        }

        /// <summary>
        /// Add a new variable,value pair to this environment.
        /// </summary>
        /// <param name="var">The variable to add</param>
        /// <param name="val">The variable value</param>
        /// <returns>The variable object</returns>
        public object Define(object var, object val) {
            Variables = Cons(var, Variables);
            Values = Cons(val, Values);

            if (val is Procedure
                && ((Procedure)val).Name.Equals("anonymous procedure")) {
                ((Procedure)val).Name = var.ToString();
            }

            return var;
        }

        /// <summary>
        /// Set the value of an existing variable
        /// </summary>
        /// <param name="var">The variable</param>
        /// <param name="val">The new value</param>
        /// <returns>The variable object or error</returns>
        public object Set(object var, object val) {
            if (!(var is string)) {
                return Error("Attempt to set a non-symbol: {0}", Stringify(var));
            }

            var symbol = (string)var;
            object varList = Variables, valList = Values;

            // See if the symbol is bound locally
            while (varList != null) {
                if ((string)First(varList) == symbol) {
                    return SetFirst(valList, val);
                }

                var restString = Rest(varList) as string;
                if (restString != null && restString == symbol) {
                    return SetRest(valList, val);
                }

                varList = Rest(varList);
                valList = Rest(valList);
            }

            // If not, try to look for the parent
            if (Parent != null) {
                return Parent.Set(symbol, val);
            }

            return Error("Unbound variable: {0}", symbol);
        }

        public Environment DefinePrimitive(string name, int id, int minArgs) {
            Define(name, new Primitive(id, minArgs, minArgs));
            return this;
        }

        public Environment DefinePrimitive(string name, int id, int minArgs, int maxArgs) {
            Define(name, new Primitive(id, minArgs, maxArgs));
            return this;
        }

        /// <summary>
        /// See if there is an appropriate number of values for these variables.
        /// </summary>
        /// <param name="variables">The variable</param>
        /// <param name="values">The values</param>
        /// <returns>True if the variables have the appropriate number of values, false otherwise</returns>
        private static bool NumberArgsOk(object variables, object values) {
            return ((variables == null && values == null)
                    || (variables is string)
                    || (variables is Pair && values is Pair
                        && NumberArgsOk(((Pair)variables).TheRest, ((Pair)values).TheRest)));
        }
    }
}