/** @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

namespace JSchemeSharp {

    public sealed class Macro : Closure {

        /// <summary>
        /// Make a macro from a parameter list, body, and environment.
        /// </summary>
        /// <param name="parameters">The parameter list</param>
        /// <param name="body">The body</param>
        /// <param name="environment">The <see cref="Environment"/></param>
        public Macro(object parameters, object body, Environment environment) : base(parameters, body, environment) {
        }

        /// <summary>
        /// Replace the old cons cell with the macro expansion, and return it.
        /// </summary>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <param name="oldPair">The old pair</param>
        /// <param name="arguments">The arguments</param>
        /// <returns>The replaced cons cell</returns>
        public Pair Expand(Scheme interpreter, Pair oldPair, object arguments) {
            var expansion = Apply(interpreter, arguments);

            if (expansion is Pair) {
                oldPair.TheFirst = ((Pair)expansion).TheFirst;
                oldPair.TheRest = ((Pair)expansion).TheRest;
            } else {
                oldPair.TheFirst = "begin";
                oldPair.TheRest = Cons(expansion, null);
            }

            return oldPair;
        }

        /// <summary>
        /// Macro expand an expression.
        /// </summary>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <param name="x">The expression object</param>
        /// <returns>The expanded object</returns>
        public static object MacroExpand(Scheme interpreter, object x) {
            if (!(x is Pair)) {
                return x;
            }

            var fn = interpreter.Evaluate(First(x), interpreter.GlobalEnvironment);

            if (!(fn is Macro)) {
                return x;
            }

            return ((Macro)fn).Expand(interpreter, (Pair)x, Rest(x));
        }
    }
}