/** A primitive is a procedure that is defined as part of the Scheme report,
 * and is implemented in Java code.
 * @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html  **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;

namespace JSchemeSharp {

    public sealed class Primitive : Procedure {

        private const int EQ = 0,
            LT = 1,
            GT = 2,
            GE = 3,
            LE = 4,
            ABS = 5,
            EOF_OBJECT = 6,
            EQQ = 7,
            EQUALQ = 8,
            FORCE = 9,
            CAR = 10,
            FLOOR = 11,
            CEILING = 12,
            CONS = 13,
            DIVIDE = 14,
            LENGTH = 15,
            LIST = 16,
            LISTQ = 17,
            APPLY = 18,
            MAX = 19,
            MIN = 20,
            MINUS = 21,
            NEWLINE = 22,
            NOT = 23,
            NULLQ = 24,
            NUMBERQ = 25,
            PAIRQ = 26,
            PLUS = 27,
            PROCEDUREQ = 28,
            READ = 29,
            CDR = 30,
            ROUND = 31,
            SECOND = 32,
            SYMBOLQ = 33,
            TIMES = 34,
            TRUNCATE = 35,
            WRITE = 36,
            APPEND = 37,
            BOOLEANQ = 38,
            SQRT = 39,
            EXPT = 40,
            REVERSE = 41,
            ASSOC = 42,
            ASSQ = 43,
            ASSV = 44,
            MEMBER = 45,
            MEMQ = 46,
            MEMV = 47,
            EQVQ = 48,
            LISTREF = 49,
            LISTTAIL = 50,
            STRINQ = 51,
            MAKESTRING = 52,
            STRING = 53,
            STRINGLENGTH = 54,
            STRINGREF = 55,
            STRINGSET = 56,
            SUBSTRING = 57,
            STRINGAPPEND = 58,
            STRINGTOLIST = 59,
            LISTTOSTRING = 60,
            SYMBOLTOSTRING = 61,
            STRINGTOSYMBOL = 62,
            EXP = 63,
            LOG = 64,
            SIN = 65,
            COS = 66,
            TAN = 67,
            ACOS = 68,
            ASIN = 69,
            ATAN = 70,
            NUMBERTOSTRING = 71,
            STRINGTONUMBER = 72,
            CHARQ = 73,
            CHARALPHABETICQ = 74,
            CHARNUMERICQ = 75,
            CHARWHITESPACEQ = 76,
            CHARUPPERCASEQ = 77,
            CHARLOWERCASEQ = 78,
            CHARTOINTEGER = 79,
            INTEGERTOCHAR = 80,
            CHARUPCASE = 81,
            CHARDOWNCASE = 82,
            STRINGQ = 83,
            VECTORQ = 84,
            MAKEVECTOR = 85,
            VECTOR = 86,
            VECTORLENGTH = 87,
            VECTORREF = 88,
            VECTORSET = 89,
            LISTTOVECTOR = 90,
            MAP = 91,
            FOREACH = 92,
            CALLCC = 93,
            VECTORTOLIST = 94,
            LOAD = 95,
            DISPLAY = 96,
            INPUTPORTQ = 98,
            CURRENTINPUTPORT = 99,
            OPENINPUTFILE = 100,
            CLOSEINPUTPORT = 101,
            OUTPUTPORTQ = 103,
            CURRENTOUTPUTPORT = 104,
            OPENOUTPUTFILE = 105,
            CLOSEOUTPUTPORT = 106,
            READCHAR = 107,
            PEEKCHAR = 108,
            EVAL = 109,
            QUOTIENT = 110,
            REMAINDER = 111,
            MODULO = 112,
            THIRD = 113,
            EOFOBJECTQ = 114,
            GCD = 115,
            LCM = 116,
            CXR = 117,
            ODDQ = 118,
            EVENQ = 119,
            ZEROQ = 120,
            POSITIVEQ = 121,
            NEGATIVEQ = 122,
            CHARCMP = 123 /* to 127 */,
            CHARCICMP = 128 /* to 132 */,
            STRINGCMP = 133 /* to 137 */,
            STRINGCICMP = 138 /* to 142 */,
            EXACTQ = 143,
            INEXACTQ = 144,
            INTEGERQ = 145,
            CALLWITHINPUTFILE = 146,
            CALLWITHOUTPUTFILE = 147
            ;

        //////////////// Extensions ////////////////

        private const int NEW = -1,
            CLASS = -2,
            METHOD = -3,
            EXIT = -4,
            SETCAR = -5,
            SETCDR = -6,
            TIMECALL = -11,
            MACROEXPAND = -12,
            ERROR = -13,
            LISTSTAR = -14
            ;

        private readonly int _idNumber;
        private readonly int _maxArgs;
        private readonly int _minArgs;

        public Primitive(int id, int minArgs, int maxArgs) {
            _idNumber = id;
            _minArgs = minArgs;
            _maxArgs = maxArgs;
        }

        public static Environment InstallPrimitives(Environment environment) {
            const int n = int.MaxValue;

            environment
                .DefinePrimitive("*", TIMES, 0, n)
                .DefinePrimitive("*", TIMES, 0, n)
                .DefinePrimitive("+", PLUS, 0, n)
                .DefinePrimitive("-", MINUS, 1, n)
                .DefinePrimitive("/", DIVIDE, 1, n)
                .DefinePrimitive("<", LT, 2, n)
                .DefinePrimitive("<=", LE, 2, n)
                .DefinePrimitive("=", EQ, 2, n)
                .DefinePrimitive(">", GT, 2, n)
                .DefinePrimitive(">=", GE, 2, n)
                .DefinePrimitive("abs", ABS, 1)
                .DefinePrimitive("acos", ACOS, 1)
                .DefinePrimitive("append", APPEND, 0, n)
                .DefinePrimitive("apply", APPLY, 2, n)
                .DefinePrimitive("asin", ASIN, 1)
                .DefinePrimitive("assoc", ASSOC, 2)
                .DefinePrimitive("assq", ASSQ, 2)
                .DefinePrimitive("assv", ASSV, 2)
                .DefinePrimitive("atan", ATAN, 1)
                .DefinePrimitive("boolean?", BOOLEANQ, 1)
                .DefinePrimitive("caaaar", CXR, 1)
                .DefinePrimitive("caaadr", CXR, 1)
                .DefinePrimitive("caaar", CXR, 1)
                .DefinePrimitive("caadar", CXR, 1)
                .DefinePrimitive("caaddr", CXR, 1)
                .DefinePrimitive("caadr", CXR, 1)
                .DefinePrimitive("caar", CXR, 1)
                .DefinePrimitive("cadaar", CXR, 1)
                .DefinePrimitive("cadadr", CXR, 1)
                .DefinePrimitive("cadar", CXR, 1)
                .DefinePrimitive("caddar", CXR, 1)
                .DefinePrimitive("cadddr", CXR, 1)
                .DefinePrimitive("caddr", THIRD, 1)
                .DefinePrimitive("cadr", SECOND, 1)
                .DefinePrimitive("call-with-current-continuation", CALLCC, 1)
                .DefinePrimitive("call-with-input-file", CALLWITHINPUTFILE, 2)
                .DefinePrimitive("call-with-output-file", CALLWITHOUTPUTFILE, 2)
                .DefinePrimitive("car", CAR, 1)
                .DefinePrimitive("cdaaar", CXR, 1)
                .DefinePrimitive("cdaadr", CXR, 1)
                .DefinePrimitive("cdaar", CXR, 1)
                .DefinePrimitive("cdadar", CXR, 1)
                .DefinePrimitive("cdaddr", CXR, 1)
                .DefinePrimitive("cdadr", CXR, 1)
                .DefinePrimitive("cdar", CXR, 1)
                .DefinePrimitive("cddaar", CXR, 1)
                .DefinePrimitive("cddadr", CXR, 1)
                .DefinePrimitive("cddar", CXR, 1)
                .DefinePrimitive("cdddar", CXR, 1)
                .DefinePrimitive("cddddr", CXR, 1)
                .DefinePrimitive("cdddr", CXR, 1)
                .DefinePrimitive("cddr", CXR, 1)
                .DefinePrimitive("cdr", CDR, 1)
                .DefinePrimitive("char->integer", CHARTOINTEGER, 1)
                .DefinePrimitive("char-alphabetic?", CHARALPHABETICQ, 1)
                .DefinePrimitive("char-ci<=?", CHARCICMP + LE, 2)
                .DefinePrimitive("char-ci<?", CHARCICMP + LT, 2)
                .DefinePrimitive("char-ci=?", CHARCICMP + EQ, 2)
                .DefinePrimitive("char-ci>=?", CHARCICMP + GE, 2)
                .DefinePrimitive("char-ci>?", CHARCICMP + GT, 2)
                .DefinePrimitive("char-downcase", CHARDOWNCASE, 1)
                .DefinePrimitive("char-lower-case?", CHARLOWERCASEQ, 1)
                .DefinePrimitive("char-numeric?", CHARNUMERICQ, 1)
                .DefinePrimitive("char-upcase", CHARUPCASE, 1)
                .DefinePrimitive("char-upper-case?", CHARUPPERCASEQ, 1)
                .DefinePrimitive("char-whitespace?", CHARWHITESPACEQ, 1)
                .DefinePrimitive("char<=?", CHARCMP + LE, 2)
                .DefinePrimitive("char<?", CHARCMP + LT, 2)
                .DefinePrimitive("char=?", CHARCMP + EQ, 2)
                .DefinePrimitive("char>=?", CHARCMP + GE, 2)
                .DefinePrimitive("char>?", CHARCMP + GT, 2)
                .DefinePrimitive("char?", CHARQ, 1)
                .DefinePrimitive("close-input-port", CLOSEINPUTPORT, 1)
                .DefinePrimitive("close-output-port", CLOSEOUTPUTPORT, 1)
                .DefinePrimitive("complex?", NUMBERQ, 1)
                .DefinePrimitive("cons", CONS, 2)
                .DefinePrimitive("cos", COS, 1)
                .DefinePrimitive("current-input-port", CURRENTINPUTPORT, 0)
                .DefinePrimitive("current-output-port", CURRENTOUTPUTPORT, 0)
                .DefinePrimitive("display", DISPLAY, 1, 2)
                .DefinePrimitive("eof-object?", EOFOBJECTQ, 1)
                .DefinePrimitive("eq?", EQQ, 2)
                .DefinePrimitive("equal?", EQUALQ, 2)
                .DefinePrimitive("eqv?", EQVQ, 2)
                .DefinePrimitive("eval", EVAL, 1, 2)
                .DefinePrimitive("even?", EVENQ, 1)
                .DefinePrimitive("exact?", INTEGERQ, 1)
                .DefinePrimitive("exp", EXP, 1)
                .DefinePrimitive("expt", EXPT, 2)
                .DefinePrimitive("force", FORCE, 1)
                .DefinePrimitive("for-each", FOREACH, 1, n)
                .DefinePrimitive("gcd", GCD, 0, n)
                .DefinePrimitive("inexact?", INEXACTQ, 1)
                .DefinePrimitive("input-port?", INPUTPORTQ, 1)
                .DefinePrimitive("integer->char", INTEGERTOCHAR, 1)
                .DefinePrimitive("integer?", INTEGERQ, 1)
                .DefinePrimitive("lcm", LCM, 0, n)
                .DefinePrimitive("length", LENGTH, 1)
                .DefinePrimitive("list", LIST, 0, n)
                .DefinePrimitive("list->string", LISTTOSTRING, 1)
                .DefinePrimitive("list->vector", LISTTOVECTOR, 1)
                .DefinePrimitive("list-ref", LISTREF, 2)
                .DefinePrimitive("list-tail", LISTTAIL, 2)
                .DefinePrimitive("list?", LISTQ, 1)
                .DefinePrimitive("load", LOAD, 1)
                .DefinePrimitive("log", LOG, 1)
                .DefinePrimitive("macro-expand", MACROEXPAND, 1)
                .DefinePrimitive("make-string", MAKESTRING, 1, 2)
                .DefinePrimitive("make-vector", MAKEVECTOR, 1, 2)
                .DefinePrimitive("map", MAP, 1, n)
                .DefinePrimitive("max", MAX, 1, n)
                .DefinePrimitive("member", MEMBER, 2)
                .DefinePrimitive("memq", MEMQ, 2)
                .DefinePrimitive("memv", MEMV, 2)
                .DefinePrimitive("min", MIN, 1, n)
                .DefinePrimitive("modulo", MODULO, 2)
                .DefinePrimitive("negative?", NEGATIVEQ, 1)
                .DefinePrimitive("newline", NEWLINE, 0, 1)
                .DefinePrimitive("not", NOT, 1)
                .DefinePrimitive("null?", NULLQ, 1)
                .DefinePrimitive("number->string", NUMBERTOSTRING, 1, 2)
                .DefinePrimitive("number?", NUMBERQ, 1)
                .DefinePrimitive("odd?", ODDQ, 1)
                .DefinePrimitive("open-input-file", OPENINPUTFILE, 1)
                .DefinePrimitive("open-output-file", OPENOUTPUTFILE, 1)
                .DefinePrimitive("output-port?", OUTPUTPORTQ, 1)
                .DefinePrimitive("pair?", PAIRQ, 1)
                .DefinePrimitive("peek-char", PEEKCHAR, 0, 1)
                .DefinePrimitive("positive?", POSITIVEQ, 1)
                .DefinePrimitive("procedure?", PROCEDUREQ, 1)
                .DefinePrimitive("quotient", QUOTIENT, 2)
                .DefinePrimitive("rational?", INTEGERQ, 1)
                .DefinePrimitive("read", READ, 0, 1)
                .DefinePrimitive("read-char", READCHAR, 0, 1)
                .DefinePrimitive("real?", NUMBERQ, 1)
                .DefinePrimitive("remainder", REMAINDER, 2)
                .DefinePrimitive("reverse", REVERSE, 1)
                .DefinePrimitive("round", ROUND, 1)
                .DefinePrimitive("set-car!", SETCAR, 2)
                .DefinePrimitive("set-cdr!", SETCDR, 2)
                .DefinePrimitive("sin", SIN, 1)
                .DefinePrimitive("sqrt", SQRT, 1)
                .DefinePrimitive("string", STRING, 0, n)
                .DefinePrimitive("string->list", STRINGTOLIST, 1)
                .DefinePrimitive("string->number", STRINGTONUMBER, 1, 2)
                .DefinePrimitive("string->symbol", STRINGTOSYMBOL, 1)
                .DefinePrimitive("string-append", STRINGAPPEND, 0, n)
                .DefinePrimitive("string-ci<=?", STRINGCICMP + LE, 2)
                .DefinePrimitive("string-ci<?", STRINGCICMP + LT, 2)
                .DefinePrimitive("string-ci=?", STRINGCICMP + EQ, 2)
                .DefinePrimitive("string-ci>=?", STRINGCICMP + GE, 2)
                .DefinePrimitive("string-ci>?", STRINGCICMP + GT, 2)
                .DefinePrimitive("string-length", STRINGLENGTH, 1)
                .DefinePrimitive("string-ref", STRINGREF, 2)
                .DefinePrimitive("string-set!", STRINGSET, 3)
                .DefinePrimitive("string<=?", STRINGCMP + LE, 2)
                .DefinePrimitive("string<?", STRINGCMP + LT, 2)
                .DefinePrimitive("string=?", STRINGCMP + EQ, 2)
                .DefinePrimitive("string>=?", STRINGCMP + GE, 2)
                .DefinePrimitive("string>?", STRINGCMP + GT, 2)
                .DefinePrimitive("string?", STRINGQ, 1)
                .DefinePrimitive("substring", SUBSTRING, 3)
                .DefinePrimitive("symbol->string", SYMBOLTOSTRING, 1)
                .DefinePrimitive("symbol?", SYMBOLQ, 1)
                .DefinePrimitive("tan", TAN, 1)
                .DefinePrimitive("vector", VECTOR, 0, n)
                .DefinePrimitive("vector->list", VECTORTOLIST, 1)
                .DefinePrimitive("vector-length", VECTORLENGTH, 1)
                .DefinePrimitive("vector-ref", VECTORREF, 2)
                .DefinePrimitive("vector-set!", VECTORSET, 3)
                .DefinePrimitive("vector?", VECTORQ, 1)
                .DefinePrimitive("write", WRITE, 1, 2)
                .DefinePrimitive("write-char", DISPLAY, 1, 2)
                .DefinePrimitive("zero?", ZEROQ, 1)

                ///////////// Extensions ////////////////
                .DefinePrimitive("new", NEW, 1)
                .DefinePrimitive("class", CLASS, 1)
                .DefinePrimitive("method", METHOD, 2, n)
                .DefinePrimitive("exit", EXIT, 0, 1)
                .DefinePrimitive("error", ERROR, 0, n)
                .DefinePrimitive("time-call", TIMECALL, 1, 2)
                .DefinePrimitive("_list*", LISTSTAR, 0, n)
                ;

            return environment;
        }

        /// <summary>
        /// Apply a primitive to a list of arguments.
        /// </summary>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <param name="arguments">The list of arguments</param>
        /// <returns>The result or error</returns>
        public override object Apply(Scheme interpreter, object arguments) {
            // First make sure there are the right number of arguments.
            var nArgs = Length(arguments);
            if (nArgs == -1) {
                return Error("Cannot count a list that contains itself.");
            }

            if (nArgs < _minArgs) {
                return Error("too few arguments, {0}, for {1}: {2}", nArgs, Name, arguments);
            }

            if (nArgs > _maxArgs) {
                return Error("too many arguments, {0}, for {1}: {2}", nArgs, Name, arguments);
            }

            var x = First(arguments);
            var y = Second(arguments);

            switch (_idNumber) {
                ////////////////  SECTION 6.1 BOOLEANS
                case NOT:
                    return x is bool && (bool)x == false;

                case BOOLEANQ:
                    return x is bool;

                ////////////////  SECTION 6.2 EQUIVALENCE PREDICATES
                case EQVQ:
                    return Eqv(x, y);

                case EQQ:
                    return x is bool && y is bool ? (bool)x == (bool)y : x == y;

                case EQUALQ:
                    return Equal(x, y);

                ////////////////  SECTION 6.3 LISTS AND PAIRS
                case PAIRQ:
                    return x is Pair;

                case LISTQ:
                    return IsList(x);

                case CXR:
                    for (var i = Name.Length - 2; i >= 1; i--) {
                        x = (Name[i] == 'a') ? First(x) : Rest(x);
                    }

                    return x;

                case CONS:
                    return Cons(x, y);

                case CAR:
                    return First(x);

                case CDR:
                    return Rest(x);

                case SETCAR:
                    return SetFirst(x, y);

                case SETCDR:
                    return SetRest(x, y);

                case SECOND:
                    return Second(x);

                case THIRD:
                    return Third(x);

                case NULLQ:
                    return x == null;

                case LIST:
                    return arguments;

                case LENGTH:
                    var length = Length(x);
                    return length == -1 ? Error("Cannot count a list that contains itself") : Num(length);

                case APPEND:
                    return (arguments == null) ? null : Append(arguments);

                case REVERSE:
                    return Reverse(x);

                case LISTTAIL:
                    for (var k = (int)Num(y); k > 0; k--) {
                        x = Rest(x);
                    }

                    return x;

                case LISTREF:
                    for (var k = (int)Num(y); k > 0; k--) {
                        x = Rest(x);
                    }

                    return First(x);

                case MEMQ:
                    return MemberAssoc(x, y, 'm', 'q');

                case MEMV:
                    return MemberAssoc(x, y, 'm', 'v');

                case MEMBER:
                    return MemberAssoc(x, y, 'm', ' ');

                case ASSQ:
                    return MemberAssoc(x, y, 'a', 'q');

                case ASSV:
                    return MemberAssoc(x, y, 'a', 'v');

                case ASSOC:
                    return MemberAssoc(x, y, 'a', ' ');

                ////////////////  SECTION 6.4 SYMBOLS
                case SYMBOLQ:
                    return x is string;

                case SYMBOLTOSTRING:
                    return Sym(x).ToCharArray();

                case STRINGTOSYMBOL:
                    return string.Intern(Stringify(x, false));

                ////////////////  SECTION 6.5 NUMBERS
                case NUMBERQ:
                    return x.IsNumber();

                case ODDQ:
                    return Math.Abs(Num(x)) % 2 != 0;

                case EVENQ:
                    return Math.Abs(Num(x)) % 2 == 0;

                case ZEROQ:
                    return Num(x) == 0;

                case POSITIVEQ:
                    return Num(x) > 0;

                case NEGATIVEQ:
                    return Num(x) < 0;

                case INTEGERQ:
                    return IsExact(x);

                case INEXACTQ:
                    return !IsExact(x);

                case LT:
                    return NumericCompare(arguments, '<');

                case GT:
                    return NumericCompare(arguments, '>');

                case EQ:
                    return NumericCompare(arguments, '=');

                case LE:
                    return NumericCompare(arguments, 'L');

                case GE:
                    return NumericCompare(arguments, 'G');

                case MAX:
                    return NumericCompute(arguments, 'X', Num(x));

                case MIN:
                    return NumericCompute(arguments, 'N', Num(x));

                case PLUS:
                    return NumericCompute(arguments, '+', 0.0);

                case MINUS:
                    return NumericCompute(Rest(arguments), '-', Num(x));

                case TIMES:
                    return NumericCompute(arguments, '*', 1.0);

                case DIVIDE:
                    return NumericCompute(Rest(arguments), '/', Num(x));

                case QUOTIENT:
                    var d = Num(x) / Num(y);
                    return Num(d > 0 ? Math.Floor(d) : Math.Ceiling(d));

                case REMAINDER:
                    return Num((long)Num(x) % (long)Num(y));

                case MODULO:
                    long xi = (long)Num(x), yi = (long)Num(y), m = xi % yi;
                    return Num((xi * yi > 0 || m == 0) ? m : m + yi);

                case ABS:
                    return Num(Math.Abs(Num(x)));

                case FLOOR:
                    return Num(Math.Floor(Num(x)));

                case CEILING:
                    return Num(Math.Ceiling(Num(x)));

                case TRUNCATE:
                    d = Num(x);
                    return Num((d < 0.0) ? Math.Ceiling(d) : Math.Floor(d));

                case ROUND:
                    return Num(Math.Round(Num(x)));

                case EXP:
                    return Num(Math.Exp(Num(x)));

                case LOG:
                    return Num(Math.Log(Num(x)));

                case SIN:
                    return Num(Math.Sin(Num(x)));

                case COS:
                    return Num(Math.Cos(Num(x)));

                case TAN:
                    return Num(Math.Tan(Num(x)));

                case ASIN:
                    return Num(Math.Asin(Num(x)));

                case ACOS:
                    return Num(Math.Acos(Num(x)));

                case ATAN:
                    return Num(Math.Atan(Num(x)));

                case SQRT:
                    return Num(Math.Sqrt(Num(x)));

                case EXPT:
                    return Num(Math.Pow(Num(x), Num(y)));

                case NUMBERTOSTRING:
                    return NumberToString(x, y);

                case STRINGTONUMBER:
                    return StringToNumber(x, y);

                case GCD:
                    return (arguments == null) ? ZERO : Gcd(arguments);

                case LCM:
                    return (arguments == null) ? ONE : Lcm(arguments);

                ////////////////  SECTION 6.6 CHARACTERS
                case CHARQ:
                    return x is char;

                case CHARALPHABETICQ:
                    return char.IsLetter(Chr(x));

                case CHARNUMERICQ:
                    return char.IsDigit(Chr(x));

                case CHARWHITESPACEQ:
                    return char.IsWhiteSpace(Chr(x));

                case CHARUPPERCASEQ:
                    return char.IsUpper(Chr(x));

                case CHARLOWERCASEQ:
                    return char.IsLower(Chr(x));

                case CHARTOINTEGER:
                    return (double)Chr(x);

                case INTEGERTOCHAR:
                    return (char)(int)Num(x);

                case CHARUPCASE:
                    return char.ToUpper(Chr(x));

                case CHARDOWNCASE:
                    return char.ToLower(Chr(x));

                case CHARCMP + EQ:
                    return CharCompare(x, y, false) == 0;

                case CHARCMP + LT:
                    return CharCompare(x, y, false) < 0;

                case CHARCMP + GT:
                    return CharCompare(x, y, false) > 0;

                case CHARCMP + GE:
                    return CharCompare(x, y, false) >= 0;

                case CHARCMP + LE:
                    return CharCompare(x, y, false) <= 0;

                case CHARCICMP + EQ:
                    return CharCompare(x, y, true) == 0;

                case CHARCICMP + LT:
                    return CharCompare(x, y, true) < 0;

                case CHARCICMP + GT:
                    return CharCompare(x, y, true) > 0;

                case CHARCICMP + GE:
                    return CharCompare(x, y, true) >= 0;

                case CHARCICMP + LE:
                    return CharCompare(x, y, true) <= 0;

                case ERROR:
                    return Error(Stringify(arguments));

                ////////////////  SECTION 6.7 STRINGS
                case STRINGQ:
                    return x is char[];

                case MAKESTRING:
                    var str = new char[(int)Num(x)];
                    if (y != null) {
                        var c = Chr(y);
                        for (var i = str.Length - 1; i >= 0; i--) {
                            str[i] = c;
                        }
                    }

                    return str;

                case STRING:
                    return ListToString(arguments);

                case STRINGLENGTH:
                    return Num(Str(x).Length);

                case STRINGREF:
                    return Str(x)[(int)Num(y)];

                case STRINGSET:
                    var z = Third(arguments);
                    Str(x)[(int)Num(y)] = Chr(z);
                    return z;

                case SUBSTRING:
                    int start = (int)Num(y), end = (int)Num(Third(arguments));
                    return new string(Str(x), start, end - start).ToCharArray();

                case STRINGAPPEND:
                    return StringAppend(arguments);

                case STRINGTOLIST:
                    Pair result = null;
                    var str2 = Procedure.Str(x);
                    for (var i = str2.Length - 1; i >= 0; i--) {
                        result = Cons(str2[i], result);
                    }

                    return result;

                case LISTTOSTRING:
                    return ListToString(x);

                case STRINGCMP + EQ:
                    return StringCompare(x, y, false) == 0;

                case STRINGCMP + LT:
                    return StringCompare(x, y, false) < 0;

                case STRINGCMP + GT:
                    return StringCompare(x, y, false) > 0;

                case STRINGCMP + GE:
                    return StringCompare(x, y, false) >= 0;

                case STRINGCMP + LE:
                    return StringCompare(x, y, false) <= 0;

                case STRINGCICMP + EQ:
                    return StringCompare(x, y, true) == 0;

                case STRINGCICMP + LT:
                    return StringCompare(x, y, true) < 0;

                case STRINGCICMP + GT:
                    return StringCompare(x, y, true) > 0;

                case STRINGCICMP + GE:
                    return StringCompare(x, y, true) >= 0;

                case STRINGCICMP + LE:
                    return StringCompare(x, y, true) <= 0;

                ////////////////  SECTION 6.8 VECTORS
                case VECTORQ:
                    return x is object[];

                case MAKEVECTOR:
                    var vec = new object[(int)Num(x)];
                    if (y != null) {
                        for (var i = 0; i < vec.Length; i++) {
                            vec[i] = y;
                        }
                    }

                    return vec;

                case VECTOR:
                    return ListToVector(arguments);

                case VECTORLENGTH:
                    return Num(Procedure.Vector(x).Length);

                case VECTORREF:
                    return Procedure.Vector(x)[(int)Num(y)];

                case VECTORSET:
                    return Procedure.Vector(x)[(int)Num(y)] = Third(arguments);

                case VECTORTOLIST:
                    return VectorToList(x);

                case LISTTOVECTOR:
                    return ListToVector(x);

                ////////////////  SECTION 6.9 CONTROL FEATURES
                case EVAL:
                    return interpreter.Evaluate(x);

                case FORCE:
                    return (!(x is Procedure))
                        ? x
                        : Procedure.Proc(x).Apply(interpreter, null);

                case MACROEXPAND:
                    return Macro.MacroExpand(interpreter, x);

                case PROCEDUREQ:
                    return x is Procedure;

                case APPLY:
                    return Proc(x).Apply(interpreter, ListStar(Rest(arguments)));

                case MAP:
                    return Map(Proc(x), Rest(arguments), interpreter, List(null));

                case FOREACH:
                    return Map(Proc(x), Rest(arguments), interpreter, null);

                case CALLCC:
                    var cc = new SystemException();
                    var proc = new Continuation(cc);
                    try {
                        return Proc(x).Apply(interpreter, List(proc));
                    } catch (SystemException e) {
                        if (e == cc) {
                            return proc.Value;
                        }

                        throw e;
                    }

                ////////////////  SECTION 6.10 INPUT AND OUPUT
                case EOFOBJECTQ:
                    return x is string && (string)x == InputPort.EOF;

                case INPUTPORTQ:
                    return x is InputPort;

                case CURRENTINPUTPORT:
                    return interpreter.Input;

                case OPENINPUTFILE:
                    return OpenInputFile(x);

                case CLOSEINPUTPORT:
                    return InPort(x, interpreter).Close();

                case OUTPUTPORTQ:
                    return x is TextWriter;

                case CURRENTOUTPUTPORT:
                    return interpreter.Output;

                case OPENOUTPUTFILE:
                    return OpenOutputFile(x);

                case CALLWITHOUTPUTFILE:
                    TextWriter p = null;
                    try {
                        p = OpenOutputFile(x);
                        z = Procedure.Proc(y).Apply(interpreter, List(p));
                    } finally {
                        if (p != null) {
                            p.Close();
                        }
                    }

                    return z;

                case CALLWITHINPUTFILE:
                    InputPort p2 = null;
                    try {
                        p2 = OpenInputFile(x);
                        z = Procedure.Proc(y).Apply(interpreter, List(p2));
                    } finally {
                        if (p2 != null) {
                            p2.Close();
                        }
                    }

                    return z;

                case CLOSEOUTPUTPORT:
                    OutPort(x, interpreter).Close();
                    return true;

                case READCHAR:
                    return InPort(x, interpreter).ReadChar();

                case PEEKCHAR:
                    return InPort(x, interpreter).PeekChar();

                case LOAD:
                    return interpreter.Load(x);

                case READ:
                    return InPort(x, interpreter).Read();

                case EOF_OBJECT:
                    return InputPort.IsEof(x);

                case WRITE:
                    return Write(x, OutPort(y, interpreter), true);

                case DISPLAY:
                    return Write(x, OutPort(y, interpreter), false);

                case NEWLINE:
                    OutPort(x, interpreter).WriteLine();
                    OutPort(x, interpreter).Flush();
                    return true;

                ////////////////  EXTENSIONS
                case CLASS:
                    try {
                        return Type.GetType(Stringify(x, false));
                    } catch (Exception) { }

                    return false;

                case NEW:
                    try {
                        return Activator.CreateInstance(Type.GetType(Stringify(x, false)));
                    } catch (Exception) { }

                    return false;

                case METHOD:
                    return new Method(Stringify(x, false), y, Rest(Rest(arguments)));

                case EXIT:
                    System.Environment.Exit((x == null) ? 0 : (int)Num(x));
                    return false;

                case LISTSTAR:
                    return ListStar(arguments);

                case TIMECALL:
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    var sw = new Stopwatch();

                    var startMem = GC.GetTotalMemory(true);
                    sw.Start();
                    object ans = false;
                    var nTimes = (y == null ? 1 : (int)Num(y));
                    for (var i = 0; i < nTimes; i++) {
                        ans = Procedure.Proc(x).Apply(interpreter, null);
                    }

                    sw.Stop();
                    var time = sw.ElapsedMilliseconds;
                    var mem = startMem - GC.GetTotalMemory(true);
                    return Cons(ans, List(List(Num(time), "msec"), List(Num(mem), "bytes")));

                default:
                    return Error("internal error: unknown primitive: {0} applied to {1}", this, arguments);
            }
        }

        public static char[] StringAppend(object arguments) {
            var result = new StringBuilder();
            for (; arguments is Pair; arguments = Rest(arguments)) {
                result.Append(Stringify(First(arguments), false));
            }

            return result.ToString().ToCharArray();
        }

        public static object MemberAssoc(object obj, object list, char m, char eq) {
            while (list is Pair) {
                var target = (m == 'm') ? First(list) : First(First(list));
                bool found;
                switch (eq) {
                    case 'q':
                        found = (target == obj);
                        break;

                    case 'v':
                        found = Eqv(target, obj);
                        break;

                    case ' ':
                        found = Equal(target, obj);
                        break;

                    default:
                        Warning("Bad option to memberAssoc: {0}", eq);
                        return false;
                }

                if (found) {
                    return (m == 'm') ? list : First(list);
                }

                list = Rest(list);
            }

            return false;
        }

        public static object NumericCompare(object args, char op) {
            while (Rest(args) is Pair) {
                var x = Num(First(args));
                args = Rest(args);
                var y = Num(First(args));
                switch (op) {
                    case '>':
                        if (!(x > y)) {
                            return false;
                        }

                        break;

                    case '<':
                        if (!(x < y)) {
                            return false;
                        }

                        break;

                    case '=':
                        if (!(x == y)) {
                            return false;
                        }

                        break;

                    case 'L':
                        if (!(x <= y)) {
                            return false;
                        }

                        break;

                    case 'G':
                        if (!(x >= y)) {
                            return false;
                        }

                        break;

                    default:
                        Error("internal error: unrecognized op: {0}", op);
                        break;
                }
            }

            return true;
        }

        public static object NumericCompute(object arguments, char op, double result) {
            if (arguments == null) {
                switch (op) {
                    case '-':
                        return Num(0 - result);

                    case '/':
                        return Num(1 / result);

                    default:
                        return Num(result);
                }
            }

            while (arguments is Pair) {
                var x = Num(First(arguments));
                arguments = Rest(arguments);
                switch (op) {
                    case 'X':
                        if (x > result) {
                            result = x;
                        }

                        break;

                    case 'N':
                        if (x < result) {
                            result = x;
                        }

                        break;

                    case '+':
                        result += x;
                        break;

                    case '-':
                        result -= x;
                        break;

                    case '*':
                        result *= x;
                        break;

                    case '/':
                        result /= x;
                        break;

                    default:
                        Error("internal error: unrecognized op: {0}", op);
                        break;
                }
            }

            return Num(result);
        }

        /// <summary>
        /// Return the sign of the argument: +1, -1, or 0.
        /// </summary>
        /// <param name="x">The integer</param>
        /// <returns>The sign</returns>
        private static int Sign(int x) {
            return (x > 0) ? +1 : (x < 0) ? -1 : 0;
        }

        /// <summary>
        /// Return less than 0 if x is alphabetically first, greater than 0 if y is first,
        /// 0 if same. Case insensitive if ci is true. Error if not both chars.
        /// </summary>
        /// <param name="x">Left side</param>
        /// <param name="y">Right side</param>
        /// <param name="ci">Case insensitive</param>
        /// <returns>The comparison result</returns>
        public static int CharCompare(object x, object y, bool ci) {
            char xc = Chr(x), yc = Chr(y);
            if (ci) {
                xc = char.ToLower(xc);
                yc = char.ToLower(yc);
            }

            return xc - yc;
        }

        /// <summary>
        /// Return less than 0 if x is alphabetically first, greather than 0 if y is first,
        /// 0 if same.Case insensitive iff ci is true.  Error if not strings.
        /// </summary>
        /// <param name="x">Left side</param>
        /// <param name="y">Right side</param>
        /// <param name="ci">Case insensitive</param>
        /// <returns>The comparison result</returns>
        public static int StringCompare(object x, object y, bool ci) {
            if (x is char[] && y is char[]) {
                char[] xc = (char[])x, yc = (char[])y;
                for (var i = 0; i < xc.Length; i++) {
                    var diff = (!ci)
                        ? xc[i] - yc[i]
                        : char.ToUpper(xc[i]) - char.ToUpper(yc[i]);

                    if (diff != 0) {
                        return diff;
                    }
                }

                return xc.Length - yc.Length;
            }

            Error("expected two strings, got: {0}", Stringify(List(x, y)));
            return 0;
        }

        private static object NumberToString(object x, object y) {
            var _base = y.IsNumber() ? (int)Num(y) : 10;
            if (_base != 10 || Num(x) == Math.Round(Num(x))) {
                // An integer
                return Convert.ToString((long)Num(x), _base).ToCharArray();
            }

            // A floating point number
            return x.ToString().ToCharArray();
        }

        private static object StringToNumber(object x, object y) {
            var _base = y.IsNumber() ? (int)Num(y) : 10;
            try {
                return (_base == 10)
                    ? Convert.ToDouble(Stringify(x, false), CultureInfo.InvariantCulture)
                    : Num(Convert.ToInt64(Stringify(x, false), _base));
            } catch (FormatException) {
            } catch (ArgumentException) {
            }

            return false;
        }

        private static object Gcd(object args) {
            long gcd = 0;
            while (args is Pair) {
                gcd = Gcd2(Math.Abs((long)Num(First(args))), gcd);
                args = Rest(args);
            }

            return Num(gcd);
        }

        private static long Gcd2(long a, long b) {
            return b == 0 ? a : Gcd2(b, a % b);
        }

        private static object Lcm(object args) {
            long L = 1, g;
            while (args is Pair) {
                var n = Math.Abs((long)Num(First(args)));
                g = Gcd2(n, L);
                L = (g == 0) ? g : (n / g) * L;
                args = Rest(args);
            }

            return Num(L);
        }

        private static bool IsExact(object x) {
            if (!(x is double)) {
                return false;
            }

            var d = Num(x);
            return (d == Math.Round(d) && Math.Abs(d) < 102962884861573423.0);
        }

        private static TextWriter OpenOutputFile(object filename) {
            try {
                return new StreamWriter(Stringify(filename, false));
            } catch (FileNotFoundException) {
                return (TextWriter)Error("No such file: {0}", Stringify(filename));
            } catch (IOException e) {
                return (TextWriter)Error("IOException: {0}", e.Message);
            }
        }

        private static InputPort OpenInputFile(object filename) {
            try {
                return new InputPort(new StreamReader(Stringify(filename, false)));
            } catch (FileNotFoundException) {
                return (InputPort)Error("No such file: {0}", Stringify(filename));
            } catch (IOException e) {
                return (InputPort)Error("IOException: {0}", e.Message);
            }
        }

        private static bool IsList(object x) {
            object slow = x, fast = x;
            for (;;) {
                if (fast == null) {
                    return true;
                }

                if (slow == Rest(fast) || !(fast is Pair)
                    || !(slow is Pair)) {
                    return false;
                }

                slow = Rest(slow);
                fast = Rest(fast);
                if (fast == null) {
                    return true;
                }

                if (!(fast is Pair)) {
                    return false;
                }

                fast = Rest(fast);
            }
        }

        private static object Append(object args) {
            return Rest(args) == null ? First(args) : Append2(First(args), Append(Rest(args)));
        }

        private static object Append2(object x, object y) {
            if (x is Pair) {
                return Cons(First(x), Append2(Rest(x), y));
            }

            return y;
        }

        /// <summary>
        /// Map proc over a list of lists of arguments, in the given interpreter. If result is non-null,
        /// accumulate the results of each call there and return that at the end. Otherwise, just
        /// return null.
        /// </summary>
        /// <param name="proc">The <see cref="Procedure"/> to map</param>
        /// <param name="args">The list of arguments</param>
        /// <param name="interp">The Scheme interpreter</param>
        /// <param name="result">The result</param>
        /// <returns>Accumulated result or null</returns>
        private static Pair Map(Procedure proc, object args, Scheme interp, Pair result) {
            var accum = result;
            if (Rest(args) == null) {
                args = First(args);
                while (args is Pair) {
                    var x = proc.Apply(interp, List(First(args)));
                    if (accum != null) {
                        accum = (Pair)(accum.TheRest = List(x));
                    }

                    args = Rest(args);
                }
            } else {
                Procedure car = Procedure.Proc(interp.Evaluate("car")), cdr = Procedure.Proc(interp.Evaluate("cdr"));
                while (First(args) is Pair) {
                    var x = proc.Apply(interp, Map(car, List(args), interp, List(null)));
                    if (accum != null) {
                        accum = (Pair)(accum.TheRest = List(x));
                    }

                    args = Map(cdr, List(args), interp, List(null));
                }
            }

            return (Pair)Rest(result);
        }
    }
}