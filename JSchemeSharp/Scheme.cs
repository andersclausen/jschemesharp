/** This class represents a Scheme interpreter.
 * See http://www.norvig.com/jscheme.html for more documentation.
 * This is version 1.4.
 * @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JSchemeSharp {

    public sealed class Scheme : SchemeUtils, IDisposable {
        public Environment GlobalEnvironment = new Environment();
        public InputPort Input = new InputPort(new StreamReader(Console.OpenStandardInput()));
        public TextWriter Output = new StreamWriter(Console.OpenStandardOutput()) { AutoFlush = true };

        private readonly Queue<InputPort> _additionalPorts = new Queue<InputPort>();

        /// <summary>
        /// Create a Scheme interpreter and load an array of files into it.
        /// Also load SchemePrimitives.CODE.
        /// </summary>
        /// <param name="files">String list of files</param>
        /// <param name="output">Optional <see cref="TextWriter"/> output</param>
        public Scheme(IList<string> files, TextWriter output = null) {
            if (output != null) {
                Output = output;
            }

            Primitive.InstallPrimitives(GlobalEnvironment);
            try {
                Load(new InputPort(SchemePrimitives.CODE));
                for (var i = 0; i < Count(files); i++) {
                    Load((object)files[i]);
                }
            } catch (SystemException e) {
                Output.WriteLine("Exception {0}", e);
            }
        }

        private static int Count<T>(ICollection<T> list) {
            return list == null ? 0 : list.Count;
        }

        //////////////// Main Loop

        /// <summary>
        /// Create a new Scheme interpreter, passing in the command line args
        /// as files to load, and then enter a read eval write loop.
        /// </summary>
        /// <param name="files">File list</param>
        public static void Main(string[] files) {
            new Scheme(files).ReadEvalWriteLoop();
        }

        /// <summary>
        /// Prompt, read, eval, and write the result.
        /// Also sets up a catch for any RuntimeExceptions encountered.
        /// </summary>
        public void ReadEvalWriteLoop() {
            for (;;) {
                try {
                    Output.Write("> ");
                    Output.Flush();
                    object x;
                    if (InputPort.IsEof(x = Input.Read())) {
                        return;
                    }

                    Write(Evaluate(x), Output, true);
                    Output.WriteLine();
                    Output.Flush();
                } catch (SystemException e) {
                    Output.WriteLine("Exception {0}", e);
                }
            }
        }

        /// <summary>
        /// Eval all the expressions in a file. Calls Load(InputPort).
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <returns>True if finished</returns>
        public object Load(object fileName) {
            var name = Stringify(fileName, false);
            try {
                var port = new InputPort(new StreamReader(name));
                _additionalPorts.Enqueue(port);
                return Load(port);
            } catch (IOException) {
                return Error("can't load {0}", name);
            }
        }

        /// <summary>
        /// Eval all the expressions coming from an InputPort.
        /// </summary>
        /// <param name="input">The input port</param>
        /// <returns>True if finished</returns>
        public object Load(InputPort input) {
            for (;;) {
                object x;
                if (InputPort.IsEof(x = input.Read())) {
                    return true;
                }

                Evaluate(x);
            }
        }

        /// <summary>
        /// Eval all the expressions coming from a string.
        /// </summary>
        /// <param name="codeString">The string</param>
        /// <returns>True if finished</returns>
        public object Load(string codeString) {
            return Load(new InputPort(codeString));
        }

        //////////////// Evaluation

        /// <summary>
        /// Evaluate an object, x, in an environment.
        /// </summary>
        /// <param name="x">The Scheme object</param>
        /// <param name="env">The <see cref="Environment"/></param>
        /// <returns>The evalutation result</returns>
        public object Evaluate(object x, Environment env) {
            // The purpose of the while loop is to allow tail recursion.
            // The idea is that in a tail recursive position, we do "x = ..."
            // and loop, rather than doing "return Evaluate(...)".
            while (true) {
                if (x is string) {
                    // VARIABLE
                    return env.Lookup((string)x);
                }

                if (!(x is Pair)) {
                    // CONSTANT
                    return x;
                }

                var fn = First(x);
                var fnString = fn is string ? (string)fn : string.Empty;

                var args = Rest(x);
                if (fnString == "quote") {
                    // QUOTE
                    return First(args);
                }

                if (fnString == "begin") {
                    // BEGIN
                    for (; Rest(args) != null; args = Rest(args)) {
                        Evaluate(First(args), env);
                    }

                    x = First(args);
                } else if (fnString == "define") {
                    // DEFINE
                    if (First(args) is Pair) {
                        return env.Define(First(First(args)),
                            Evaluate(Cons("lambda", Cons(Rest(First(args)), Rest(args))), env));
                    }

                    return env.Define(First(args), Evaluate(Second(args), env));
                } else if (fnString == "set!") {
                    // SET!
                    return env.Set(First(args), Evaluate(Second(args), env));
                } else if (fnString == "if") {
                    // IF
                    x = (Truth(Evaluate(First(args), env))) ? Second(args) : Third(args);
                } else if (fnString == "cond") {
                    // COND
                    x = ReduceCond(args, env);
                } else if (fnString == "lambda") {
                    // LAMBDA
                    return new Closure(First(args), Rest(args), env);
                } else if (fnString == "macro") {
                    // MACRO
                    return new Macro(First(args), Rest(args), env);
                } else {
                    // PROCEDURE CALL:
                    fn = Evaluate(fn, env);
                    fnString = fn is string ? (string)fn : string.Empty;

                    if (fn is Macro) {
                        // (MACRO CALL)
                        x = ((Macro)fn).Expand(this, (Pair)x, args);
                    } else if (fn is Closure) {
                        // (CLOSURE CALL)
                        var f = (Closure)fn;
                        x = f.Body;
                        env = new Environment(f.Parameters, EvalList(args, env), f.Environment);
                    } else {
                        // (OTHER PROCEDURE CALL)
                        return Procedure.Proc(fn).Apply(this, EvalList(args, env));
                    }
                }
            }
        }

        /// <summary>
        /// /** Eval in the global environment. **/
        /// </summary>
        /// <param name="x">The Scheme object</param>
        /// <returns>The evaluation result</returns>
        public object Evaluate(object x) {
            return Evaluate(x, GlobalEnvironment);
        }

        /// <summary>
        /// Evaluate each of a list of expressions.
        /// </summary>
        /// <param name="list">The list of expressions</param>
        /// <param name="environment">The <see cref="Environment"/></param>
        /// <returns>The evaluation result</returns>
        private Pair EvalList(object list, Environment environment) {
            if (list == null) {
                return null;
            }

            if (!(list is Pair)) {
                Error("Illegal arg list: {0}", list);
                return null;
            }

            return Cons(Evaluate(First(list), environment), EvalList(Rest(list), environment));
        }

        /// <summary>
        /// Bind a method on a class object to the global environment. Use this when the/an object is known and should be defined.
        /// </summary>
        /// <param name="classInstance">The class instance</param>
        /// <param name="methodName">The C# method name</param>
        /// <param name="functionSchemeName">The Scheme function name</param>
        /// <param name="classInstanceSchemeName">The Scheme object name</param>
        /// <param name="argumentTypes">A variable number of argument types for the method</param>
        public void BindMethod(object classInstance, string methodName, string functionSchemeName, string classInstanceSchemeName, params Type[] argumentTypes) {
            BindMethod(classInstance, methodName, functionSchemeName, classInstanceSchemeName, GlobalEnvironment, argumentTypes);
        }

        /// <summary>
        /// Bind a method on a class object to an environment. Use this when the/an object is known and should be defined.
        /// </summary>
        /// <param name="classInstance">The class instance</param>
        /// <param name="methodName">The C# method name</param>
        /// <param name="functionSchemeName">The Scheme function name</param>
        /// <param name="classInstanceSchemeName">The Scheme object name</param>
        /// <param name="environment">The environment to add bind to</param>
        /// <param name="argumentTypes">A variable number of argument types for the method</param>
        public void BindMethod(object classInstance, string methodName, string functionSchemeName, string classInstanceSchemeName, Environment environment, params Type[] argumentTypes) {
            environment.Define(classInstanceSchemeName, classInstance);
            BindMethod(classInstance.GetType(), methodName, functionSchemeName, environment, argumentTypes);
        }

        /// <summary>
        /// Bind a method on a class type to the global environment. The class objects have to be defined separately.
        /// </summary>
        /// <param name="classType">The class type</param>
        /// <param name="methodName">The C# method name</param>
        /// <param name="functionSchemeName">The Scheme function name</param>
        /// <param name="environment">The environment to add bind to</param>
        /// <param name="argumentTypes">A variable number of argument types for the method</param>
        public void BindMethod(Type classType, string methodName, string functionSchemeName, params Type[] argumentTypes) {
            BindMethod(classType, methodName, functionSchemeName, GlobalEnvironment, argumentTypes);
        }

        /// <summary>
        /// Bind a method on a class type to an environment. The class objects have to be defined separately.
        /// </summary>
        /// <param name="classType">The class type</param>
        /// <param name="methodName">The C# method name</param>
        /// <param name="functionSchemeName">The Scheme function name</param>
        /// <param name="environment">The environment to add bind to</param>
        /// <param name="argumentTypes">A variable number of argument types for the method</param>
        public void BindMethod(Type classType, string methodName, string functionSchemeName, Environment environment, params Type[] argumentTypes) {
            var sb = new StringBuilder();
            sb.Append("(define ").Append(functionSchemeName).Append(" (method \"").Append(methodName).Append("\"");
            sb.Append(" \"").Append(SchemeUtils.QualifiedName(classType)).Append("\"");

            foreach (var type in argumentTypes) {
                sb.Append(" \"").Append(SchemeUtils.QualifiedName(type)).Append("\"");
            }

            sb.Append("))");
            Load(sb.ToString());
        }

        /// <summary>
        /// Reduce a cond expression to some code which, when evaluated,
        /// gives the value of the cond expression. We do it that way to
        /// maintain tail recursion.
        /// </summary>
        /// <param name="clauses">The clauses</param>
        /// <param name="environment">The <see cref="Environment"/></param>
        /// <returns>The reduced cond</returns>
        private object ReduceCond(object clauses, Environment environment) {
            object result = null;

            for (;;) {
                if (clauses == null) {
                    return false;
                }

                var clause = First(clauses);
                clauses = Rest(clauses);

                var firstClause = First(clause) as string;
                if (firstClause != null && firstClause == "else" || Truth(result = Evaluate(First(clause), environment))) {
                    if (Rest(clause) == null) {
                        return List("quote", result);
                    }

                    var secondClause = Second(clause) as string;
                    if (secondClause != null && secondClause == "=>") {
                        return List(Third(clause), List("quote", result));
                    }

                    return Cons("begin", Rest(clause));
                }
            }
        }

        public void Dispose() {
            while (_additionalPorts.Count > 0) {
                var port = _additionalPorts.Dequeue();
                port.Dispose();
            }
        }
    }
}