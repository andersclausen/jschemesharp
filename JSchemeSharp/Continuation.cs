/** @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html */

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

using System;

namespace JSchemeSharp {

    public sealed class Continuation : Procedure {
        private readonly SystemException _cc;
        public object Value { get; set; }

        public Continuation(SystemException cc) {
            _cc = cc;
        }

        public override object Apply(Scheme interpreter, object arguments) {
            Value = First(arguments);
            throw _cc;
        }
    }
}