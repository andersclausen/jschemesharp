/** A Pair has two fields, first and rest (or car and cdr).
 * The empty list is represented by null. The methods that you might
 * expect here, like first, second, list, etc. are instead static methods
 * in class SchemeUtils.
 * @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html */

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

using System.Collections.Generic;
using System.Text;

namespace JSchemeSharp {

    public sealed class Pair : SchemeUtils {

        /// <summary>
        /// The first element of the pair.
        /// </summary>
        public object TheFirst { get; set; }

        /// <summary>
        /// The other element of the pair.
        /// </summary>
        public object TheRest { get; set; }

        /// <summary>
        /// Build a pair from two components.
        /// </summary>
        /// <param name="first">The first in the pair</param>
        /// <param name="rest">The second in the pair</param>
        public Pair(object first, object rest) {
            TheFirst = first;
            TheRest = rest;
        }

        /// <summary>
        /// Two pairs are equal if their first and rest fields are equal.
        /// </summary>
        /// <param name="x">Param to compare</param>
        /// <returns>The truth value of the comparison</returns>
        public override bool Equals(object x) {
            if (x == this) {
                return true;
            }

            var that = x as Pair;
            if (that == null) {
                return false;
            }

            return Equal(TheFirst, that.TheFirst) && Equal(TheRest, that.TheRest);
        }

        /// <summary>
        /// Return a string representation of the pair.
        /// </summary>
        /// <returns>A string representation of the pair</returns>
        public override string ToString() {
            return Stringify(this, true);
        }

        /// <summary>
        /// Build up a string representation of the Pair in a StringBuilder.
        /// </summary>
        /// <param name="quoted">If quoted is true, use "str" and #\c, otherwise use str and c</param>
        /// <param name="buffer">The StringBuilder buffer</param>
        public void StringifyPair(bool quoted, StringBuilder buffer) {
            if (ContainsLoops()) {
                buffer.Append("** error: Unable to extract string from Pair - it contains loops");
                return;
            }

            string special = null;
            if ((TheRest is Pair) && Rest(TheRest) == null) {
                var theFirstString = TheFirst is string ? (string)TheFirst : string.Empty;
                special = (theFirstString == "quote")
                    ? "'"
                    : (theFirstString == "quasiquote")
                        ? "`"
                        : (theFirstString == "unquote")
                            ? ","
                            : (theFirstString == "unquote-splicing")
                                ? ",@"
                                : null;
            }

            if (special != null) {
                buffer.Append(special);
                Stringify(Second(this), quoted, buffer);
            } else {
                buffer.Append('(');
                Stringify(TheFirst, quoted, buffer);
                var tail = TheRest;
                while (tail is Pair) {
                    buffer.Append(' ');
                    Stringify(((Pair)tail).TheFirst, quoted, buffer);
                    tail = ((Pair)tail).TheRest;
                }

                if (tail != null) {
                    buffer.Append(" . ");
                    Stringify(tail, quoted, buffer);
                }

                buffer.Append(')');
            }
        }

        public void StringifyPairLooped(bool quoted, StringBuilder buffer, Stack<StringifyArgs> stack) {
            if (ContainsLoopsLooped()) {
                buffer.Append("** error: Unable to extract string from Pair - it contains loops");
                return;
            }

            string special = null;
            if ((TheRest is Pair) && Rest(TheRest) == null) {
                var theFirstString = TheFirst is string ? (string)TheFirst : string.Empty;
                special = (theFirstString == "quote")
                    ? "'"
                    : (theFirstString == "quasiquote")
                        ? "`"
                        : (theFirstString == "unquote")
                            ? ","
                            : (theFirstString == "unquote-splicing")
                                ? ",@"
                                : null;
            }

            if (special != null) {
                buffer.Append(special);
                stack.Push(new StringifyArgs(Second(this), quoted, buffer));
            } else {
                buffer.Append('(');

                var reversedStack = new Stack<StringifyArgs>();

                reversedStack.Push(new StringifyArgs(TheFirst, quoted, buffer));
                var tail = TheRest;

                while (tail is Pair) {
                    reversedStack.Push(new StringifyArgs(buffer, (sb) => { sb.Append(' '); }));
                    reversedStack.Push(new StringifyArgs(((Pair)tail).TheFirst, quoted, buffer));
                    tail = ((Pair)tail).TheRest;
                }

                if (tail != null) {
                    reversedStack.Push(new StringifyArgs(buffer, (sb) => { sb.Append(" . "); }));
                    reversedStack.Push(new StringifyArgs(tail, quoted, buffer));
                }

                reversedStack.Push(new StringifyArgs(buffer, (sb) => { sb.Append(')'); }));

                // Add to actual stack in reverse order
                while (reversedStack.Count > 0) {
                    stack.Push(reversedStack.Pop());
                }
            }
        }

        private bool ContainsLoops(HashSet<Pair> seen) {
            if (seen.Contains(this)) {
                return true;
            }

            seen.Add(this);
            var pair = TheFirst as Pair;
            if (pair != null) {
                if (pair.ContainsLoops(seen)) {
                    return true;
                }
            }

            pair = TheRest as Pair;
            if (pair != null) {
                return pair.ContainsLoops(seen);
            }

            seen.Remove(this);

            return false;
        }

        private bool ContainsLoopsLooped() {
            var rest = TheRest as Pair;
            if (rest == null) {
                return false;
            }

            var seen = new HashSet<Pair>();
            seen.Add(this);

            while (true) {
                if (seen.Contains(rest)) {
                    return true;
                }

                seen.Add(rest);
                rest = rest.TheRest as Pair;

                if (rest == null) {
                    return false;
                }
            }
        }

        /// <summary>
        /// Determine if a Pair contains itself in any of its branches.
        /// </summary>
        /// <returns>True if the Pair contains itself.</returns>
        private bool ContainsLoops() {
            return ContainsLoops(new HashSet<Pair>());
        }

        /// <summary>
        /// Get the pair hash code.
        /// </summary>
        /// <returns>The hash code</returns>
        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}