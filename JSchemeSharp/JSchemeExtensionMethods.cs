/**  @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
* This altered version of JScheme is licenced under the MIT license:
* https://opensource.org/licenses/MIT as allowed under point 4 of the
* original license.
*/
using System.IO;

namespace JSchemeSharp {
    public static class JSchemeExtensionMethods {

        public static Stream GenerateStreamFromString(this string s) {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static bool IsNumber(this object value) {
            return value is sbyte
                   || value is byte
                   || value is short
                   || value is ushort
                   || value is int
                   || value is uint
                   || value is long
                   || value is ulong
                   || value is float
                   || value is double
                   || value is decimal;
        }
    }
}