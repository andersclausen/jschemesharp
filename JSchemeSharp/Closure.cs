/** A closure is a user-defined procedure.  It is "closed" over the
 * environment in which it was created.  To apply the procedure, bind
 * the parameters to the passed in variables, and evaluate the body.
 * @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html **/

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

namespace JSchemeSharp {

    /// <summary>
    /// A closure is a user-defined procedure.  It is "closed" over the
    /// environment in which it was created.To apply the procedure, bind
    /// the parameters to the passed in variables, and evaluate the body.
    /// </summary>
    public class Closure : Procedure {
        public object Body { get; set; }
        public Environment Environment { get; set; }
        public object Parameters { get; set; }

        /// <summary>
        /// Make a closure from a parameter list, body, and environment.
        /// </summary>
        /// <param name="parameters">The parameter list</param>
        /// <param name="body">The body</param>
        /// <param name="environment">The environment</param>
        public Closure(object parameters, object body, Environment environment) {
            Parameters = parameters;
            Environment = environment;
            Body = (body is Pair && Rest(body) == null)
                ? First(body)
                : Cons("begin", body);
        }

        /// <summary>
        /// Apply a closure to a list of arguments.
        /// </summary>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <param name="arguments">The arguments</param>
        /// <returns>The eval result</returns>
        public override object Apply(Scheme interpreter, object arguments) {
            return interpreter.Evaluate(Body, new Environment(Parameters, arguments, Environment));
        }
    }
}