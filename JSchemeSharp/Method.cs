/** @author Peter Norvig, peter@norvig.com http://www.norvig.com
 * Copyright 1998 Peter Norvig, see http://www.norvig.com/license.html */

/* C# conversion by Anders Clausen (jaczehack+jschemesharp(AT)gmail.com)
 * This altered version of JScheme is licenced under the MIT license:
 * https://opensource.org/licenses/MIT as allowed under point 4 of the
 * original license.
 */

using System;
using System.Collections.Generic;
using System.Reflection;

namespace JSchemeSharp {

    /// <summary>
    /// A method class.
    /// (This was named "JavaMethod" in the original code.)
    /// </summary>
    public class Method : Procedure {
        private readonly Type[] _argClasses;
        private readonly bool _isStatic;
        private readonly MethodInfo _method;
        private static readonly Dictionary<string, Type> TypeMap =
            new Dictionary<string, Type> {
                        { "void", typeof (void) },
                        { "bool", typeof (bool) },
                        { "char", typeof (char) },
                        { "int", typeof (int) },
                        { "byte", typeof (byte) },
                        { "short", typeof (short) },
                        { "long", typeof (long) },
                        { "float", typeof (float) },
                        { "double", typeof (double) },
            };

        public Method(string methodName, object targetClassName, object argClassNames) {
            Name = Stringify(targetClassName, false) + "." + methodName;

            try {
                _argClasses = ClassArray(argClassNames);
                _method = ToClass(targetClassName).GetMethod(methodName, _argClasses);
                _isStatic = _method.IsStatic;
            } catch (ArgumentException) {
                Error("Bad class, can't get method {0}", Name);
            } catch (AmbiguousMatchException) {
                Error("Can't get method {0}", Name);
            }
        }

        /// <summary>
        /// Apply the method to a list of arguments.
        /// </summary>
        /// <param name="interpreter">The Scheme interpreter</param>
        /// <param name="arguments">The list of argumetns</param>
        /// <returns>The result or error</returns>
        public override object Apply(Scheme interpreter, object arguments) {
            try {
                return _isStatic ? _method.Invoke(null, ToArray(arguments)) : _method.Invoke(First(arguments), ToArray(Rest(arguments)));
            } catch (TargetException e) {
                System.Diagnostics.Debug.WriteLine("Apply: " + e.ToString());
            } catch (ArgumentException e) {
                System.Diagnostics.Debug.WriteLine("Apply: " + e.ToString());
            } catch (TargetInvocationException e) {
                System.Diagnostics.Debug.WriteLine("Apply: " + e.ToString());
            } catch (MethodAccessException e) {
                System.Diagnostics.Debug.WriteLine("Apply: " + e.ToString());
            } catch (InvalidOperationException e) {
                System.Diagnostics.Debug.WriteLine("Apply: " + e.ToString());
            } catch (NotSupportedException e) {
                System.Diagnostics.Debug.WriteLine("Apply: " + e.ToString());
            }

            return Error("Bad method application: {0} {1}, ", this, Stringify(arguments));
        }

        public static Type ToClass(object arg) {
            if (arg.GetType() == typeof(Type)) {
                return (Type)arg;
            }

            var sarg = Stringify(arg, false);
            if (TypeMap.ContainsKey(sarg)) {
                return TypeMap[sarg];
            }

            return Type.GetType(sarg);
        }

        /// <summary>
        /// Convert a list of Objects into an array. Peek at the argClasses array to see what's
        /// expected. That enables us to convert between double and Integer, something Java won't do automatically.
        /// </summary>
        /// <param name="args">The arguments</param>
        /// <returns>The object array</returns>
        public object[] ToArray(object args) {
            var n = Length(args);
            if (n == -1) {
                Error("Cannot count a list that contains itself");
            }

            var diff = n - _argClasses.Length;
            if (diff != 0) {
                Error("{0} too {1} args to {2}", Math.Abs(diff), (diff > 0) ? "many" : "few", Name);
            }

            var array = new object[n];
            for (var i = 0; i < n && i < _argClasses.Length; i++) {
                array[i] = Convert.ChangeType(First(args), _argClasses[i]);
                args = Rest(args);
            }

            return array;
        }

        /// <summary>
        /// Convert a list of class names into an array of Classes.
        /// </summary>
        /// <param name="args">The class names</param>
        /// <returns>The <see cref="Type"/> array</returns>
        public Type[] ClassArray(object args) {
            var n = Length(args);
            if (n == -1) {
                Error("Cannot count a list that contains itself.");
            }
            var array = new Type[n];
            for (var i = 0; i < n; i++) {
                array[i] = ToClass(First(args));
                args = Rest(args);
            }

            return array;
        }
    }
}