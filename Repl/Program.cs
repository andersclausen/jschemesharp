﻿using JSchemeSharp;

namespace Repl {

    internal class Program {

        private static void Main(string[] args) {
            Scheme.Main(args);
        }
    }
}