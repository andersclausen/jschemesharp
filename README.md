# README #

This is a C# conversion of Peter Norvig's [JScheme](http://norvig.com/jscheme.html).

*Jscheme is my implementation of Scheme in Java. This page presents Jscheme version 1.4, the last version that I released (in April 1998). If you want a mercilessly small, easily modifiable version, this is it.* --Peter Norvig

### What is it? ###

* Quick and dirty C# conversion.
* Test project and and repl project.
* .Net 3.5 for mono/Unity3D compatibility.

### How do I get set up? ###

* Clone and open in visual studio.
* Run tests or repl project.

### Contribution guidelines ###

* Be consistent in code style (I may change it at will)
* Keep it .Net 3.5/Unity3D compatible.
* New functionality must have unit tests.
* Old tests must be green.

### What to do ###

* There is probably -a lot- of tweaks that can be done even with .Net 3.5 C#

### Licensing ###

This modified package is under the [MIT license](https://opensource.org/licenses/MIT) as allowed in point four of the [original license](http://norvig.com/license.html). License files are included in the source repository.